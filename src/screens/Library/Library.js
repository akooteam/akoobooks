import React, { Component } from "react";
import { View, Alert, StyleSheet } from "react-native";
import { Container, H3, Text, Tabs, Tab } from "native-base";
import { withNavigationFocus } from "react-navigation";
import MainHeader from "../../components/MainHeader";
import AudiobookService from "../../services/AudiobookService";
import UserService from "../../services/UserService";
import { observer, inject } from "mobx-react";
import Books from "../Books";
import EmptyState from "../../components/EmptyState";
import SubscriptionService from "../../services/SubscriptionService";
import Colors from "../../helpers/Colors";

@inject("rootStore")
@observer
class Library extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      // The screen is focused
      // Call any action
      this._fetchOverApi();
      this._fetchSubscriptionBooks();
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }

  /** Make request to api */
  _fetchOverApi = () => {
    const { navigate } = this.props.navigation;
    const { audiobookStore, userStore } = this.props.rootStore;

    try {
      if (userStore.userToken) {
        this.setState({
          loading: true
        });
        UserService.setToken(userStore.userToken);

        AudiobookService.fetchAudiobooks().then(res => {
          this.setState({
            loading: false
          });
          audiobookStore.createAudiobook(res);
        });
      }
    } catch (error) {
      Alert.alert(
        "Error",
        "Sign to view and manage your library",
        [{ text: "OK", onPress: () => navigate("Account") }],
        { cancelable: true }
      );
    }
  };

  /** get subscription books */
  _fetchSubscriptionBooks = () => {
    const { subscriptionStore, userStore } = this.props.rootStore;

    try {
      if (userStore.userToken) {
        this.setState({
          loading: true
        });
        UserService.setToken(userStore.userToken);

        SubscriptionService.getBooks().then(res => {
          this.setState({
            loading: false
          });
          subscriptionStore.createAudiobook(res);
        });
      }
    } catch (_) {}
  };

  render() {
    const { loading } = this.state;
    const { audiobooks } = this.props.rootStore.audiobookStore;
    const { subscriptions } = this.props.rootStore.subscriptionStore;

    return (
      <Container>
        <MainHeader title="Library" />
        <Tabs tabBarUnderlineStyle={{ backgroundColor: Colors.lightBlue }}>
          <Tab
            heading="Audiobooks"
            tabStyle={styles.tab}
            textStyle={{ color: "#6b6b6b" }}
            activeTabStyle={styles.activeTab}
            activeTextStyle={{ color: Colors.lightBlue }}>
            {audiobooks.length == 0 ? (
              <EmptyState
                title="Your library is empty"
                subtext="Browse to find something to add to your library"
              />
            ) : (
              <Books
                navigation={this.props.navigation}
                data={audiobooks}
                refreshing={loading}
                _onRefresh={this._fetchOverApi}
              />
            )}
          </Tab>
          <Tab
            heading="Subscriptions"
            tabStyle={styles.tab}
            activeTabStyle={styles.activeTab}
            textStyle={{ color: "#6b6b6b" }}
            activeTextStyle={{ color: Colors.lightBlue }}>
            {subscriptions.length == 0 ? (
              <EmptyState
                title="No Subscriptions at this time"
                subtext="Browse to find something to add to your subscriptions"
              />
            ) : (
              <Books
                navigation={this.props.navigation}
                data={subscriptions}
                refreshing={loading}
                _onRefresh={this._fetchSubscriptionBooks}
              />
            )}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  tab: {
    backgroundColor: "#f8f8f8",
    borderColor: "#a7a6ab"
  },
  activeTab: {
    backgroundColor: "#f8f8f8",
    borderColor: "#007aff",
    color: "#007aff"
  }
});

export default withNavigationFocus(Library);
