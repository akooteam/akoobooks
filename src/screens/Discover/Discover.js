import React, { Component } from "react";
import { View, Alert, Platform } from "react-native";
import {
  Container,
  Tab,
  Tabs,
  Text,
  ScrollableTab,
  Toast,
  Button
} from "native-base";
import { inject, observer } from "mobx-react";
import styles from "./styles";
import MainHeader from "../../components/MainHeader";
import Colors from "../../helpers/Colors";
import Books from "../Books";
import BookService from "../../services/BookService";

@inject("rootStore")
@observer
class Discover extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      newReleases: [],
      biography: [],
      fiction: [],
      non_fiction: [],
      kids: []
    };
  }

  componentWillMount() {
    this._fetchOverApi(0);
  }

  /** Make request to api */
  _fetchOverApi = key => {
    switch (key) {
      case 4:
        BookService.getBooks(5)
          .then(res => {
            this.setState({
              kids: res
            });
          })
          .catch(err => {
            Alert.alert("Error", err.message);
          });
        break;

      case 1:
        BookService.getBooks(3)
          .then(res => {
            this.setState({
              biography: res
            });
          })
          .catch(err => {
            Alert.alert("Error", err.message);
          });
        break;

      case 2:
        BookService.getBooks(1)
          .then(res => {
            this.setState({
              fiction: res
            });
          })
          .catch(err => {
            Alert.alert("Error", err.message);
          });
        break;

      case 3:
        BookService.getBooks(24)
          .then(res => {
            this.setState({
              non_fiction: res
            });
          })
          .catch(err => {
            Alert.alert("Error", err.message);
          });
        break;

      default:
        BookService.newReleases()
          .then(res => {
            this.setState({
              newReleases: res
            });
          })
          .catch(err => {
            Alert.alert("Error", err.message);
          });
        break;
    }

    this.setState({
      loading: false
    });
  };

  _showSubscriptionCount = () => {
    const { subscriptionCount } = this.props.rootStore;

    Alert.alert(
      "AkooBooks Credits",
      `You have ${subscriptionCount} subscription credits available`
    );
  };

  render() {
    const { subscriptionCount } = this.props.rootStore;
    return (
      <Container style={styles.container}>
        <MainHeader
          title="Discover"
          hasTabs
          right={
            <Button
              rounded
              bordered
              style={{ backgroundColor: "white" }}
              onPress={this._showSubscriptionCount}>
              <Text>{subscriptionCount}</Text>
            </Button>
          }
        />
        <Tabs
          onChangeTab={page => this._fetchOverApi(page.i)}
          renderTabBar={() => (
            <ScrollableTab
              tabsContainerStyle={{ backgroundColor: Colors.blue }}
              underlineStyle={{ backgroundColor: Colors.lightBlue }}
            />
          )}>
          <Tab
            ref="new_releases"
            heading="New Releases"
            tabStyle={{ backgroundColor: "transparent" }}
            activeTabStyle={{ backgroundColor: "transparent" }}
            activeTextStyle={{ color: "white" }}
            textStyle={{ color: "white" }}>
            <Books
              loading={this.state.loading}
              onRefresh={this._fetchOverApi}
              data={this.state.newReleases}
              navigation={this.props.navigation}
            />
          </Tab>
          <Tab
            ref="biography"
            heading="Biography"
            tabStyle={{ backgroundColor: "transparent" }}
            activeTabStyle={{ backgroundColor: "transparent" }}
            activeTextStyle={{ color: "white" }}
            textStyle={{ color: "white" }}>
            <Books
              loading={this.state.loading}
              onRefresh={this._fetchOverApi}
              data={this.state.biography}
              navigation={this.props.navigation}
            />
          </Tab>
          <Tab
            ref="fiction"
            heading="Fiction"
            tabStyle={{ backgroundColor: "transparent" }}
            activeTabStyle={{ backgroundColor: "transparent" }}
            activeTextStyle={{ color: "white" }}
            textStyle={{ color: "white" }}>
            <Books
              loading={this.state.loading}
              onRefresh={this._fetchOverApi}
              data={this.state.fiction}
              navigation={this.props.navigation}
            />
          </Tab>
          <Tab
            ref="non_fiction"
            heading="Non-fiction"
            tabStyle={{ backgroundColor: "transparent" }}
            activeTabStyle={{ backgroundColor: "transparent" }}
            activeTextStyle={{ color: "white" }}
            textStyle={{ color: "white" }}>
            <Books
              loading={this.state.loading}
              onRefresh={this._fetchOverApi}
              data={this.state.non_fiction}
              navigation={this.props.navigation}
            />
          </Tab>
          <Tab
            ref="kids"
            heading="Kids"
            tabStyle={{ backgroundColor: "transparent" }}
            activeTabStyle={{ backgroundColor: "transparent" }}
            activeTextStyle={{ color: "white" }}
            textStyle={{ color: "white" }}>
            <Books
              loading={this.state.loading}
              onRefresh={this._fetchOverApi}
              data={this.state.kids}
              navigation={this.props.navigation}
            />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

export default Discover;
