import React, { Component } from "react";
import { View, FlatList } from "react-native";
import {
  Container,
  Button,
  Icon,
  Left,
  Right,
  Content,
  Segment,
  Text,
  List,
  ListItem
} from "native-base";
import { withNavigationFocus } from "react-navigation";
import MainHeader from "../../components/MainHeader";
import TrackPlayer from "react-native-track-player";
import Colors from "../../helpers/Colors";
import styles from "../Playlist/styles";
import { observer, inject } from "mobx-react";

@inject("rootStore")
@observer
class Playlist extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { navigation } = this.props;

    this.focusListener = navigation.addListener("didFocus", async () => {
      // The screen is focused
      // Call any action
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }

  _playTrack = async track => {
    let tracks = await TrackPlayer.getQueue();
    const { setTrack, currentTrack } = this.props.rootStore.playerStore;

    if (tracks.length == 0) {
      TrackPlayer.add(currentTrack.playlist);
    }

    TrackPlayer.skip(track.item.id);

    await setTrack(track.item);

    await TrackPlayer.play();
  };

  _renderItem = item => {
    const { track } = this.props.rootStore.playerStore;

    return (
      <ListItem
        selected={track.id == item.item.id}
        last
        onPress={() => this._playTrack(item)}>
        <Left>
          <Text>{item.item.title}</Text>
        </Left>
        <Right>
          <Text>{item.item.duration}</Text>
        </Right>
      </ListItem>
    );
  };

  _renderLeft = back => (
    <Button transparent icon onPress={() => back()}>
      <Icon name="close" style={{ color: "white" }} />
    </Button>
  );

  render() {
    const { playlist } = this.props.rootStore.playerStore.currentTrack;
    const { goBack } = this.props.navigation;

    return (
      <Container>
        <MainHeader left={this._renderLeft(goBack)} title="Playlist" />
        <Content
          contentContainerStyle={{ flex: 1, justifyContent: "space-between" }}>
          <View>
            {/* <View style={styles.downloadContainer}>
              <Text>0 of {playlist.length} tracks downloaded</Text>
              <Button small light>
                <Text>Download</Text>
              </Button>
            </View> */}
            <FlatList
              data={playlist}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => item.id}
              style={styles.list}
            />
          </View>
          {/* <View
            style={{
              paddingHorizontal: 10,
              marginVertical: 10
            }}>
            <Button small bordered danger block>
              <Text>Delete all tracks</Text>
            </Button>
          </View> */}
        </Content>
      </Container>
    );
  }
}

export default withNavigationFocus(Playlist);
