import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  downloadContainer: {
    padding: 10,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  list: {
    borderTopColor: "lightgrey",
    borderTopWidth: StyleSheet.hairlineWidth
  }
});

export default styles;
