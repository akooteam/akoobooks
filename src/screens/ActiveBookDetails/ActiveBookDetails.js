import React, { Component } from "react";
import { View, Text } from "react-native";
import styles from "./styles";
import { Container, Button, Icon, Toast } from "native-base";
import MainHeader from "../../components/MainHeader";
import BookService from "../../services/BookService";
import BookContent from "../../components/BookContent";
import { observer, inject } from "mobx-react";
import Colors from "../../helpers/Colors";

const Spinkit = require("react-native-spinkit");

@inject("rootStore")
@observer
class ActiveBookDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      refreshing: false,
      data: {}
    };
  }

  componentDidMount() {
    const { loading } = this.state;
    const { currentTrack } = this.props.rootStore.playerStore;

    BookService.getSingleBook(currentTrack.id)
      .then(res => {
        this.setState({
          loading: !loading,
          data: res
        });
      })
      .catch(e => {
        Toast.show({
          type: "danger",
          text: e.message,
          position: "top"
        });
      });
  }

  _renderLeft = goBack => (
    <Button transparent icon onPress={() => goBack()}>
      <Icon name="close" style={{ color: "white" }} />
    </Button>
  );

  _onRefresh = () => {
    const { refreshing } = this.state;
    const { currentTrack } = this.props.rootStore.playerStore;

    this.setState({
      refreshing: !refreshing
    });

    BookService.getSingleBook(currentTrack.id)
      .then(res => {
        this.setState({
          refreshing: false,
          data: res
        });
      })
      .catch(e => {
        this.setState({
          refreshing: false
        });
        Toast.show({
          type: "danger",
          text: e.message,
          position: "top"
        });
      });
  };

  render() {
    const { navigation } = this.props;
    const { loading, data, refreshing } = this.state;
    const { goBack } = this.props.navigation;
    const { currentTrack } = this.props.rootStore.playerStore;

    return (
      <Container>
        <MainHeader
          left={this._renderLeft(goBack)}
          title={currentTrack.title}
        />
        {loading ? (
          <Spinkit
            type="ChasingDots"
            color={Colors.blue}
            style={{ alignSelf: "center", marginTop: 50 }}
          />
        ) : (
          <BookContent
            options={false}
            navigation={navigation}
            data={data}
            refreshing={refreshing}
            _onRefresh={this._onRefresh}
          />
        )}
      </Container>
    );
  }
}

export default ActiveBookDetails;
