import { StyleSheet } from "react-native";
import Colors from "../../helpers/Colors";
import Layout from "../../helpers/Layout";

const styles = StyleSheet.create({
  title: {
    marginVertical: 5
  },
  artist: {
    fontWeight: "bold"
  },
  bar: {
    height: 2,
    backgroundColor: Colors.yellow
  },
  options: {
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "lightgrey",
    justifyContent: "space-between"
  },
  optionText: {
    color: Colors.black
  },
  progress: {
    height: 1,
    width: "90%",
    marginTop: 10,
    flexDirection: "row",
    alignSelf: "center"
  },
  slider: {
    width: "90%",
    alignSelf: "center"
  },
  timeContainer: {
    width: "90%",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  position: {
    textAlign: "left"
  },
  duration: {
    textAlign: "right"
  },
  cover: {
    width: Layout.window.width * 0.7,
    height: Layout.window.width * 0.7,
    alignSelf: "center",
    marginTop: 30,
    justifyContent: "flex-end"
  },
  modal: {
    justifyContent: "space-between",
    width: Layout.window.width * 0.9,
    height: null,
    borderRadius: 10
  },
  header: {
    padding: 15,
    textAlign: "center"
  },
  buttonText: {
    textTransform: "capitalize"
  },
  button: { width: "40%", padding: 15 }
});

export default styles;
