import React, { Component } from "react";
import { View, TouchableOpacity, Platform } from "react-native";
import TrackPlayer from "react-native-track-player";
import { Button, Icon, Text } from "native-base";
import Colors from "../../helpers/Colors";
import { observer, inject } from "mobx-react";

@inject("rootStore")
@observer
class PlayerControls extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _renderPlayIcon = () => {
    const { playbackState } = this.props.rootStore.playerStore;

    return (
      <Icon
        name={Platform.select({
          ios: playbackState == TrackPlayer.STATE_PLAYING ? "pause" : "play",
          android:
            playbackState == TrackPlayer.STATE_PLAYING || playbackState == 1
              ? "pause"
              : "play"
        })}
        type="FontAwesome"
        style={{ fontSize: 60, color: Colors.blue }}
      />
    );
  };

  render() {
    const { props } = this;
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          paddingHorizontal: 20
        }}>
        <TouchableOpacity onPress={props.seekBackward}>
          <Icon
            name="backward"
            type="FontAwesome"
            style={{ color: Colors.blue }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={props.skipToPrevious}>
          <Icon
            name="step-backward"
            type="FontAwesome"
            style={{ fontSize: 35, color: Colors.blue }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={props.middleButton}>
          {this._renderPlayIcon()}
        </TouchableOpacity>
        <TouchableOpacity onPress={props.skipToNext}>
          <Icon
            name="step-forward"
            type="FontAwesome"
            style={{ fontSize: 35, color: Colors.blue }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={props.seekForward}>
          <Icon
            name="forward"
            type="FontAwesome"
            style={{ color: Colors.blue }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default PlayerControls;
