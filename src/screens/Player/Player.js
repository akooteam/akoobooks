import React, { Component } from "react";
import {
  View,
  Alert,
  ImageBackground,
  InteractionManager,
  TouchableOpacity
} from "react-native";
import { Container, Text, Button, Icon, H2, List, ListItem } from "native-base";
import TrackPlayer, { ProgressComponent } from "react-native-track-player";
import BackgroundTimer from "react-native-background-timer";
import Modal from "react-native-modalbox";
import Slider from "react-native-slider";
import moment from "moment";
import { observer, inject } from "mobx-react/native";
import MainHeader from "../../components/MainHeader";
import PlayerControls from "./Controls";
import Colors from "../../helpers/Colors";
import styles from "./styles";

const timerConstants = [8, 15, 20, 30, 45];

@inject("rootStore")
@observer
class ProgressBar extends ProgressComponent {
  state = {
    duration: 0,
    position: 0,
    isSeeking: false,
    seek: 0
  };

  formatTime = timeInSec => {
    let mins = parseInt(timeInSec / 60);
    let secs = parseInt(Math.round((timeInSec % 60) * 100) / 100);
    if (mins < 10) {
      mins = "0" + mins;
    }
    if (secs < 10) {
      secs = "0" + secs;
    }
    return mins + ":" + secs;
  };

  render() {
    return (
      <View style={{ marginTop: 10 }}>
        <Slider
          style={styles.slider}
          thumbTintColor={Colors.yellow}
          minimumValue={0}
          maximumValue={this.state.duration}
          animationType="timing"
          thumbStyle={{ width: 10, height: 10 }}
          minimumTrackTintColor={Colors.yellow}
          maximumTrackTintColor="lightgrey"
          onValueChange={val => {
            TrackPlayer.pause();
            TrackPlayer.seekTo(val);
          }}
          onSlidingComplete={async () => {
            await TrackPlayer.play();
            TrackPlayer.setRate(this.props.rootStore.playerStore.speed);
          }}
          value={this.state.position}
        />
        <View style={styles.timeContainer}>
          <Text style={styles.position}>
            {this.formatTime(this.state.position)}
          </Text>
          <Text style={styles.duration}>
            {this.formatTime(this.state.duration)}
          </Text>
        </View>
      </View>
    );
  }
}

@inject("rootStore")
@observer
class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _renderLeftNav = () => {
    const { goBack } = this.props.navigation;

    return (
      <Button transparent icon onPress={() => goBack()}>
        <Icon name="arrow-down" style={{ color: "white" }} />
      </Button>
    );
  };

  _renderRightNav = () => {
    const { navigate } = this.props.navigation;

    return (
      <View style={{ flexDirection: "row" }}>
        <Button transparent icon onPress={() => navigate("Playlist")}>
          <Icon name="list" style={{ color: "white" }} />
        </Button>
      </View>
    );
  };

  seekForward = async () => {
    try {
      TrackPlayer.seekTo((await TrackPlayer.getPosition()) + 30);
    } catch (_) {}
  };

  seekBackward = async () => {
    try {
      TrackPlayer.seekTo((await TrackPlayer.getPosition()) - 30);
    } catch (_) {}
  };

  skipToNext = async () => {
    try {
      await TrackPlayer.skipToNext();
    } catch (_) {}
  };

  skipToPrevious = async () => {
    try {
      await TrackPlayer.skipToPrevious();
    } catch (_) {}
  };

  togglePlayback = async () => {
    const { track } = this.props.rootStore.playerStore;
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack == null) {
      await TrackPlayer.add(track);

      await TrackPlayer.play();
      TrackPlayer.setRate(this.props.rootStore.playerStore.speed);
    } else {
      const { playbackState } = this.props.rootStore.playerStore;
      if (playbackState == TrackPlayer.STATE_PLAYING) {
        await TrackPlayer.pause();
      } else {
        await TrackPlayer.play();
        TrackPlayer.setRate(this.props.rootStore.playerStore.speed);
      }
    }
  };

  _setSpeed = () => {
    const { playerStore } = this.props.rootStore;

    playerStore.setSpeed();
    TrackPlayer.setRate(playerStore.speed);
  };

  _openSleepModal = () => {
    InteractionManager.runAfterInteractions(() => {
      this.props.rootStore.playerStore.toggleModal();
    });
  };

  _stopTimer = async () => {
    await this.props.rootStore.playerStore.setTimer(0);
    BackgroundTimer.stopBackgroundTimer();
  };

  _cancelTimer = () => {
    Alert.alert("", "Sleep Timer", [
      { text: "Cancel Timer", onPress: this._stopTimer, style: "cancel" },
      { text: "Continue" }
    ]);
  };

  _setTimer = time => {
    let secs = time * 60000;

    let { playerStore } = this.props.rootStore;

    playerStore.setTimer(secs);

    BackgroundTimer.runBackgroundTimer(async () => {
      if (playerStore.timer == 0) {
        await TrackPlayer.pause();
        await this._stopTimer();
        return;
      }

      playerStore.setTimer(playerStore.timer - 1000);
    }, 1000);

    playerStore.toggleModal();
  };

  render() {
    const { navigate } = this.props.navigation;
    const {
      speed,
      modal,
      timer,
      track,
      currentTrack
    } = this.props.rootStore.playerStore;
    return (
      <Container>
        <MainHeader
          left={this._renderLeftNav()}
          right={this._renderRightNav()}
          title={currentTrack.title}
        />
        <View
          style={{
            flex: 1,
            justifyContent: "space-between"
          }}>
          <View>
            <ImageBackground
              source={{
                uri: track.artwork
              }}
              style={styles.cover}>
              <Button
                transparent
                icon
                large
                style={{ alignSelf: "flex-end" }}
                onPress={() => navigate("ActiveBookDetails")}>
                <Icon
                  type="MaterialCommunityIcons"
                  name="information"
                  style={{ fontSize: 35, color: Colors.blue, marginBottom: 10 }}
                />
              </Button>
            </ImageBackground>
            <ProgressBar />
          </View>
          <View style={{ alignItems: "center" }}>
            <Text style={styles.title}>{track.title}</Text>
            <Text note style={styles.artist}>
              {track.artist}
            </Text>
          </View>
          <PlayerControls
            seekBackward={this.seekBackward}
            seekForward={this.seekForward}
            playbackState={this.props.rootStore.playerStore.playbackState}
            middleButton={this.togglePlayback}
            skipToPrevious={this.skipToPrevious}
            skipToNext={this.skipToNext}
          />
          <View style={styles.options}>
            <TouchableOpacity
              style={styles.button}
              onPress={this._openSleepModal}>
              <Text style={styles.optionText}>Sleep</Text>
            </TouchableOpacity>
            {timer !== 0 && (
              <TouchableOpacity
                onPress={this._cancelTimer}
                style={styles.button}>
                <Text style={{ color: Colors.blue, textAlign: "center" }}>
                  {moment(timer)
                    .format("mm:ss")
                    .toString()}
                </Text>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              style={styles.button}
              onPress={() => this._setSpeed()}>
              <Text style={[styles.optionText, { textAlign: "right" }]}>
                Speed {speed}x
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Modal
          isOpen={modal}
          ref={"sleepModal"}
          style={styles.modal}
          position={"center"}>
          <H2 style={styles.header}>Sleep Timer</H2>
          {timerConstants.map((v, i) => (
            <Button transparent block key={i} onPress={() => this._setTimer(v)}>
              <Text style={styles.buttonText}>{v} min</Text>
            </Button>
          ))}
          <Button transparent block onPress={this._openSleepModal}>
            <Text style={[styles.buttonText, { color: "black" }]}>Cancel</Text>
          </Button>
        </Modal>
      </Container>
    );
  }
}

export default Player;
