import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  accountContainer: {
    paddingHorizontal: 16
  },
  input: {
    marginVertical: 10,
    backgroundColor: "white",
    borderRadius: 5
  },
  header: {
    alignItems: "center",
    marginTop: 40,
    marginBottom: 20
  },
  iconStyle: {
    fontSize: 28,
    marginLeft: 15
  }
});

export default styles;
