import React, { Component } from "react";
import { View, Alert } from "react-native";
import {
  Container,
  Text,
  Button,
  Form,
  Item,
  Icon,
  Input,
  H2,
  Toast,
  Content
} from "native-base";
import MainHeader from "../../components/MainHeader";
import { observer, inject } from "mobx-react";
import UserService from "../../services/UserService";
import Colors from "../../helpers/Colors";
import styles from "./styles";
import Spinner from "react-native-loading-spinner-overlay";

const Spinkit = require("react-native-spinkit");

@inject("rootStore")
@observer
class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      email: "",
      password: ""
    };
  }

  _validate = () => {
    const re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim;
    if (this.state.email == "" || !re.test(this.state.email)) {
      Toast.show({
        text: "Provide a valid email address.",
        type: "warning",
        duration: 3000,
        position: "top"
      });

      return true;
    }

    if (this.state.password.length < 6) {
      Toast.show({
        text: "Password doesn't must be six characters or more",
        type: "warning",
        duration: 3000,
        position: "top"
      });
      return true;
    }

    return false;
  };

  _login = () => {
    if (this._validate()) return;

    this.setState({
      loading: true
    });

    const { email, password } = this.state;

    UserService.login({ email, password })
      .then(res => {
        this.setState({
          loading: !this.state.loading
        });

        if (res) {
          this.props.rootStore.userStore.createUser(res);

          this.props.navigation.navigate("Profile");

          return;
        }

        throw { message: "Email and password incorrect" };
      })
      .catch(error => {
        this.setState({
          loading: false
        });

        Toast.show({
          type: "danger",
          text: error.message,
          position: "top"
        });
      });
  };

  render() {
    const { navigate, goBack } = this.props.navigation;
    const { loading } = this.state;

    return (
      <Container style={{ backgroundColor: "#f5f5f5" }}>
        <MainHeader
          title="Sign In"
          left={
            <Button transparent icon onPress={() => goBack()}>
              <Icon name="arrow-back" style={{ color: "white" }} />
            </Button>
          }
        />
        <Content style={{ paddingHorizontal: 16 }}>
          <Spinner
            visible={loading}
            customIndicator={<Spinkit type="ChasingDots" color={Colors.blue} />}
          />
          <View style={styles.header}>
            <H2>Sign In</H2>
          </View>
          <Form style={{ marginVertical: 10 }}>
            <Item style={styles.input} regular>
              <Icon active name="mail" />
              <Input
                autoCapitalize={"none"}
                placeholder="Email"
                placeholderTextColor="grey"
                keyboardType="email-address"
                onChangeText={email => this.setState({ email })}
              />
            </Item>
            <Item style={styles.input} regular>
              <Icon active name="lock" />
              <Input
                placeholder="Password"
                placeholderTextColor="grey"
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
          </Form>
          <Button
            block
            rounded
            style={{ backgroundColor: Colors.lightBlue }}
            onPress={this._login}>
            <Text>Sign In</Text>
          </Button>
          <Button
            style={{ alignSelf: "center", marginTop: 10 }}
            transparent
            onPress={() => navigate("PasswordReset")}>
            <Text style={{ color: Colors.lightBlue }}>Forgot Password?</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Login;
