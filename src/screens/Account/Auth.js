import React, { Component } from "react";
import { View } from "react-native";
import { Button, Text, Icon, Container, Content } from "native-base";
import MainHeader from "../../components/MainHeader";
import styles from "./styles";
import Colors from "../../helpers/Colors";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Container>
        <MainHeader
          title="Account"
          right={
            <Button transparent icon onPress={() => navigate("Settings")}>
              <Icon name="settings" style={{ color: "white" }} />
            </Button>
          }
        />
        <Content>
          <View style={styles.accountContainer}>
            <Icon
              name="contact"
              style={{
                fontSize: 150,
                marginVertical: 20,
                color: Colors.green,
                textAlign: "center"
              }}
            />
            <Button
              block
              rounded
              style={{ marginBottom: 15, backgroundColor: Colors.lightBlue }}
              onPress={() => navigate("Login")}>
              <Text>Sign In</Text>
            </Button>
            <Button
              block
              rounded
              bordered
              dark
              onPress={() => navigate("Register")}>
              <Text>Create an Account</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

export default Auth;
