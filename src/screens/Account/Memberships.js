import React, { Component } from "react";
import { View, FlatList, Alert, Linking, AsyncStorage } from "react-native";
import {
  Container,
  Content,
  Button,
  Icon,
  Text,
  ListItem,
  Left,
  Right,
  Body,
  Thumbnail
} from "native-base";
import _ from "lodash";
import MainHeader from "../../components/MainHeader";
import { observer, inject } from "mobx-react";
import SubscriptionService from "../../services/SubscriptionService";

@inject("rootStore")
@observer
export default class Memberships extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentWillMount() {
    this._fetchPackages();
  }

  _keyExtractor = (item, index) => `${item.id}`;

  _onPressItem = url => {
    const { user } = this.props.rootStore.userStore;
    const { navigate } = this.props.navigation

    if (!user) return Alert.alert("Error", "Login to view membership plans");

    url = _.replace(url, "{user_id}", `${user.id}`);

    navigate('WebView', {title: 'Membership Plans', link: url})

    // try {
    //   Linking.openURL(url).catch(err => Alert.alert("An error occurred", err));
    // } catch (error) {
    //   Alert.alert("Error", error.message);
    // }
  };

  _fetchPackages = () => {
    SubscriptionService.getPackages()
      .then(res => {
        this.setState({
          data: res
        });
      })
      .catch(e => {
        Alert.alert("Error", e.message);
      });
  };

  _renderItem = ({ item }) => (
    <ListItem thumbnail>
      <Left>
        <Thumbnail square source={{ uri: item.subscription_image }} />
      </Left>
      <Body>
        <Text>{item.title}</Text>
        <Text note numberOfLines={4}>
          {item.description}
        </Text>
        <Text note># of Books: {item.count}</Text>
        <Text note style={{ fontWeight: "500", color: "grey" }}>
          {item.currency} {item.amount}
        </Text>
      </Body>
      <Right>
        <Button transparent onPress={() => this._onPressItem(item.payment_url)}>
          <Text>Subscribe</Text>
        </Button>
      </Right>
    </ListItem>
  );

  render() {
    const { data } = this.state;
    const { goBack } = this.props.navigation;

    return (
      <Container>
        <MainHeader
          title="Membership Plans"
          left={
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-back" style={{ color: "white" }} />
              <Text style={{ color: "white" }}>Back</Text>
            </Button>
          }
        />
        <Content>
          <FlatList
            data={data}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
          />
        </Content>
      </Container>
    );
  }
}
