import React, { Component } from "react";
import { View, Linking, Platform } from "react-native";
import MainHeader from "../../components/MainHeader";
import {
  Container,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Separator,
  ListItem
} from "native-base";
import firebase from "react-native-firebase";
import { NavigationEvents } from "react-navigation";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subscription: false
    };
  }

  componentWillMount() {
    firebase
      .config()
      .fetch(0)
      .then(() => {
        return firebase.config().activateFetched();
      })
      .then(activated => {
        if (!activated) console.log("Fetched data not activated");
        return firebase.config().getValue("subscription");
      })
      .then(snapshot => {
        const enableSubscription = snapshot.val();

        if (enableSubscription) {
          this._enableSubscripton();
        }
      })
      .catch(console.error);
  }

  _enableSubscripton = () => {
    this.setState({
      subscription: true
    });
  };

  _openLink = url => {
    Linking.openURL(url).catch(err => console.error("An error occurred", err));
  };

  render() {
    const { goBack, navigate } = this.props.navigation;
    const { subscription } = this.state;

    return (
      <Container>
        <MainHeader
          title="Settings"
          left={
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-back" style={{ color: "white" }} />
              <Text style={{ color: "white" }}>Back</Text>
            </Button>
          }
        />
        <Content>
          <Separator bordered>
            <Text>HELP</Text>
          </Separator>
          {Platform.OS == "ios" && subscription && (
            <ListItem icon onPress={() => navigate("Memberships")}>
              <Body>
                <Text>Membership Plans</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          )}
          {Platform.OS == "android" && (
            <ListItem icon onPress={() => navigate("Memberships")}>
              <Body>
                <Text>Membership Plans</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          )}
          <ListItem
            icon
            onPress={() =>
              navigate("WebView", {
                title: "FAQs",
                link: "http://akoobooks.com/faqs"
              })
            }>
            <Body>
              <Text>FAQs</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() =>
              this._openLink(
                "mailto:info@akoobooks.com?subject=Akoobooks Feedback"
              )
            }>
            <Body>
              <Text>Email feedback</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem icon onPress={() => this._openLink("tel:+233202040176")}>
            <Body>
              <Text>Call us</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() =>
              this._openLink("https://www.facebook.com/akoobooks")
            }>
            <Body>
              <Text>Facebook</Text>
            </Body>
            <Right>
              <Text>akoobooks</Text>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            icon
            last
            onPress={() => this._openLink("https://twitter.com/@akoobooks")}>
            <Body>
              <Text>Twitter</Text>
            </Body>
            <Right>
              <Text>@akoobooks</Text>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() =>
              this._openLink("https://www.linkedin.com/company/akoobooks-audio")
            }>
            <Body>
              <Text>LinkedIn</Text>
            </Body>
            <Right>
              <Text>akoobooks-audio</Text>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            icon
            onPress={() =>
              this._openLink("https://www.instagram.com/akoobooks")
            }>
            <Body>
              <Text>Instagram</Text>
            </Body>
            <Right>
              <Text>akoobooks</Text>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <Separator bordered>
            <Text>ABOUT</Text>
          </Separator>
          <ListItem
            icon
            onPress={() =>
              navigate("WebView", {
                title: "Terms of Service",
                link: "http://akoobooks.com/terms"
              })
            }>
            <Body>
              <Text>Terms of Service</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            icon
            last
            onPress={() =>
              navigate("WebView", {
                title: "Privacy Policy",
                link: "http://akoobooks.com/privacy-policy"
              })
            }>
            <Body>
              <Text>Privacy Policy</Text>
            </Body>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

export default Settings;
