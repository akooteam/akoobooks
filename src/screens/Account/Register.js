import React, { Component } from "react";
import { View, Alert, TouchableOpacity, Modal } from "react-native";
import {
  Container,
  Content,
  Button,
  Text,
  Form,
  Item,
  Input,
  Icon,
  H2,
  Toast
} from "native-base";
import MainHeader from "../../components/MainHeader";
import UserService from "../../services/UserService";
import { observer, inject } from "mobx-react/native";
import _ from "lodash";
import Colors from "../../helpers/Colors";
import styles from "./styles";
import Spinner from "react-native-loading-spinner-overlay";
import CountryList from "../../components/CountryList";

const countryData = require("../../assets/country_dial_info");
const Spinkit = require("react-native-spinkit");

const defaultCountry = countryData.filter(obj => obj.name === "Ghana")[0];

@inject("rootStore")
@observer
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      first_name: "",
      last_name: "",
      email: "",
      phone_number: "",
      password: "",
      country: defaultCountry,
      isVisible: false
    };
  }

  _validate = () => {
    if (this.state.first_name.length < 2 && this.state.last_name.length < 2) {
      Toast.show({
        text: "First and Last Name required or invalid",
        type: "warning",
        position: "top",
        duration: 3000
      });

      return true;
    }

    const re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim;
    if (this.state.email == "" || !re.test(this.state.email)) {
      Toast.show({
        text: "Provide a valid email address.",
        type: "warning",
        duration: 3000,
        position: "top"
      });
      return true;
    }

    if (this.state.phone_number.length < 7) {
      Toast.show({
        text: "Provide a valid phone number",
        type: "warning",
        duration: 3000,
        position: "top"
      });
      return true;
    }

    if (this.state.password.length < 6) {
      Toast.show({
        text: "Password doesn't must be six characters or more",
        type: "warning",
        duration: 3000,
        position: "top"
      });
      return true;
    }

    return false;
  };

  _register = () => {
    if (this._validate()) return;

    this.setState({
      loading: true
    });

    const { first_name, last_name, phone, email, password } = this.state;

    let payload = {
      first_name,
      last_name,
      phone_number: phone,
      email,
      password
    };

    try {
      UserService.register(payload)
        .then(res => {
          this.setState({
            loading: false
          });

          if (res) {
            this.props.rootStore.userStore.createUser(res);
            this.props.navigation.navigate("Profile");
            return;
          }

          throw { message: "Something went wrong during the sign up process" };
        })
        .catch(error => {
          this.setState({
            loading: false
          });
          Toast.show({
            text: error.message,
            type: "danger",
            position: "top"
          });
        });
    } catch (error) {
      Alert.alert("Error", JSON.stringify(error));
    }
  };

  _toggleCountryModal = () => {
    const { isVisible } = this.state;
    this.setState({
      isVisible: !isVisible
    });
  };

  _setCountry = country => {
    const { phone_number } = this.state;
    const { dial_code } = country;
    let phone = `${dial_code}${phone_number}`;
    this.setState({
      country,
      phone
    });
  };

  _setPhoneNumber = phone_number => {
    const { dial_code } = this.state.country;
    let phone = `${dial_code}${phone_number}`;
    this.setState({ phone_number, phone });
  };

  render() {
    const { goBack } = this.props.navigation;
    const { loading } = this.state;

    return (
      <Container style={{ backgroundColor: "#f5f5f5" }}>
        <MainHeader
          title="Create Account"
          left={
            <Button transparent icon onPress={() => goBack()}>
              <Icon name="arrow-back" style={{ color: "white" }} />
            </Button>
          }
        />
        <Content style={{ paddingHorizontal: 16 }}>
          <Spinner
            visible={loading}
            customIndicator={<Spinkit type="ChasingDots" color={Colors.blue} />}
          />
          <Modal visible={this.state.isVisible} animationType="slide">
            <CountryList
              toggleModal={this._toggleCountryModal}
              setCountry={this._setCountry}
            />
          </Modal>
          <View style={styles.header}>
            <H2>Create Account</H2>
          </View>
          <Form style={{ marginVertical: 10 }}>
            <Item style={styles.input} regular>
              <Icon active name="person" />
              <Input
                placeholder="First Name"
                placeholderTextColor="grey"
                onChangeText={first_name => this.setState({ first_name })}
              />
            </Item>
            <Item style={styles.input} regular>
              <Icon active name="person" />
              <Input
                placeholder="Last Name"
                placeholderTextColor="grey"
                onChangeText={last_name => this.setState({ last_name })}
              />
            </Item>
            <Item style={styles.input} regular>
              <Icon active name="mail" />
              <Input
                autoCapitalize="none"
                placeholder="Email"
                placeholderTextColor="grey"
                keyboardType="email-address"
                onChangeText={email => this.setState({ email })}
              />
            </Item>
            <Item style={styles.input} regular>
              <TouchableOpacity
                onPress={this._toggleCountryModal}
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginLeft: 10,
                  marginTop: 2
                }}>
                <Text
                  style={{
                    fontSize: 17
                  }}>{`(${this.state.country.flag} ${this.state.country.dial_code})`}</Text>
              </TouchableOpacity>
              <Input
                placeholder="Phone Number"
                placeholderTextColor="grey"
                keyboardType="number-pad"
                maxLength={10}
                value={this.state.phone_number}
                onChangeText={phone_number =>
                  this._setPhoneNumber(phone_number)
                }
              />
            </Item>
            <Item style={styles.input} regular>
              <Icon active name="lock" />
              <Input
                placeholder="Password"
                placeholderTextColor="grey"
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
          </Form>
          <Button
            block
            rounded
            onPress={this._register}
            disabled={loading}
            style={{ backgroundColor: Colors.lightBlue, marginBottom: 20 }}>
            <Text>Create Account</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Register;
