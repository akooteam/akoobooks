import React, { Component } from "react";
import { View, TouchableOpacity, Platform } from "react-native";
import {
  Content,
  Text,
  Icon,
  H3,
  Button,
  Toast,
  Container,
  ListItem,
  Body
} from "native-base";
import firebase from "react-native-firebase";
import UserService from "../../services/UserService";
import { observer, inject } from "mobx-react";
import Colors from "../../helpers/Colors";
import MainHeader from "../../components/MainHeader";
import Spinner from "react-native-loading-spinner-overlay";

const Spinkit = require("react-native-spinkit");

@inject("rootStore")
@observer
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      subscription: false
    };
  }

  componentWillMount() {
    firebase
      .config()
      .fetch(0)
      .then(() => {
        return firebase.config().activateFetched();
      })
      .then(activated => {
        if (!activated) console.log("Fetched data not activated");
        return firebase.config().getValue("subscription");
      })
      .then(snapshot => {
        const enableSubscription = snapshot.val();

        if (enableSubscription) {
          this._enableSubscripton();
        }

        // continue booting app
      })
      .catch(console.error);
  }

  _enableSubscripton = () => {
    this.setState({
      subscription: true
    });
  };

  _logout = () => {
    const { navigate } = this.props.navigation;
    const { userStore } = this.props.rootStore;
    const { loading } = this.state;

    UserService.setToken(userStore.userToken);

    this.setState({
      loading: !loading
    });

    UserService.logout()
      .then(() => {
        this.setState({
          loading: !loading
        });
        navigate("Auth");
        userStore.logout();
      })
      .catch(e => {
        Toast.show({
          type: "danger",
          text: e.message,
          position: "top"
        });
      });
  };

  render() {
    const { navigate } = this.props.navigation;
    const { loading, subscription } = this.state;
    const { user, getMembershipPlan } = this.props.rootStore.userStore;
    const { audiobookStore, favoriteStore } = this.props.rootStore;
    return (
      <Container>
        <MainHeader
          title="Account"
          right={
            <Button transparent icon onPress={() => navigate("Settings")}>
              <Icon name="settings" style={{ color: "white" }} />
            </Button>
          }
        />
        <Content
          contentContainerStyle={{ flex: 1, backgroundColor: "#f5f5f5" }}>
          <Spinner
            visible={loading}
            customIndicator={<Spinkit type="ChasingDots" color={Colors.blue} />}
          />
          <View style={{ backgroundColor: "white" }}>
            <Icon
              name="contact"
              style={{
                fontSize: 150,
                marginVertical: 10,
                color: Colors.green,
                textAlign: "center"
              }}
            />
            <View style={{ alignSelf: "center", marginBottom: 20 }}>
              <H3
                style={{
                  textAlign: "center"
                }}>{`${user.first_name} ${user.last_name}`}</H3>
              <Text style={{ color: "grey", textAlign: "center" }}>
                {user.email}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              borderBottomWidth: 0.5,
              borderTopWidth: 0.5,
              borderColor: "grey",
              justifyContent: "space-evenly"
            }}>
            <TouchableOpacity
              onPress={() => navigate("Library")}
              style={{
                paddingVertical: 10
              }}>
              <Text style={{ textAlign: "center" }}>
                {audiobookStore.count}
              </Text>
              <Text style={{ textAlign: "center", color: "grey" }}>
                Audiobooks
              </Text>
            </TouchableOpacity>
            <View
              style={{
                paddingVertical: 10,
                borderRightWidth: 0.5,
                borderRightColor: "grey"
              }}
            />
            <TouchableOpacity
              onPress={() => navigate("Favorites")}
              style={{
                paddingVertical: 10
              }}>
              <Text style={{ textAlign: "center" }}>{favoriteStore.count}</Text>
              <Text style={{ textAlign: "center", color: "grey" }}>
                Favorites
              </Text>
            </TouchableOpacity>
          </View>
          {Platform.OS == "ios" && subscription && (
            <ListItem noIndent onPress={() => navigate("Memberships")}>
              <Body>
                <Text>Current Membership Plan</Text>
                <Text note>{getMembershipPlan}</Text>
              </Body>
            </ListItem>
          )}
          {Platform.OS == "android" && (
            <ListItem noIndent onPress={() => navigate("Memberships")}>
              <Body>
                <Text>Current Membership Plan</Text>
                <Text note>{getMembershipPlan}</Text>
              </Body>
            </ListItem>
          )}
          <View style={{ paddingHorizontal: 16, marginVertical: 30 }}>
            <Button block bordered rounded dark onPress={this._logout}>
              <Text>Sign Out</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

export default Profile;
