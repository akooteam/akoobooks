import React, { Component } from "react";
import { View, Alert } from "react-native";
import {
  Container,
  Button,
  Text,
  Form,
  Item,
  Input,
  Icon,
  H2,
  Toast
} from "native-base";
import MainHeader from "../../components/MainHeader";
import Colors from "../../helpers/Colors";
import styles from "./styles";
import UserService from "../../services/UserService";

class PasswordReset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      loading: false
    };
  }

  _toggleLoading = () => {
    this.setState({
      loading: !this.state.loading
    });
  };

  _resetPassword = () => {
    const { email } = this.state;
    if (!email)
      return Toast.show({
        text: "Email is required",
        type: "warning"
      });

    this._toggleLoading();

    UserService.forgotPassword({ email })
      .then(res => {
        this.setState({
          loading: false
        });

        Alert.alert(
          "Password Reset",
          "Check your email for instructions on how create a new password"
        );
      })
      .catch(e => {
        this._toggleLoading();
        Alert.alert("Error", e.message);
      });
  };

  render() {
    const { loading } = this.state;
    const { goBack } = this.props.navigation;

    return (
      <Container style={{ backgroundColor: "#f5f5f5" }}>
        <MainHeader
          title="Forgot Password"
          left={
            <Button transparent icon onPress={() => goBack()}>
              <Icon name="arrow-back" style={{ color: "white" }} />
            </Button>
          }
        />
        <View style={{ paddingHorizontal: 16 }}>
          <View style={styles.header}>
            <H2>Forgot Password</H2>
            <Text style={{ textAlign: "center", color: "grey", marginTop: 10 }}>
              We can help you out. Enter your email address and we will send you
              an email with the reset link
            </Text>
          </View>
          <Form style={{ marginVertical: 10 }}>
            <Item style={styles.input} regular>
              <Icon active name="mail" />
              <Input
                autoCompleteType="email"
                autoCorrect={false}
                autoCapitalize="none"
                onChangeText={email => this.setState({ email })}
                placeholder="Email"
                placeholderTextColor="grey"
                keyboardType="email-address"
              />
            </Item>
          </Form>
          <Button
            disabled={loading}
            block
            rounded
            style={{ backgroundColor: Colors.lightBlue }}
            onPress={this._resetPassword}>
            <Text>Submit</Text>
          </Button>
        </View>
      </Container>
    );
  }
}

export default PasswordReset;
