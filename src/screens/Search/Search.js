import React, { Component } from "react";
import { View } from "react-native";
import {
  Container,
  Header,
  Item,
  Input,
  Icon,
  Button,
  Text,
  Subtitle,
  H3
} from "native-base";
import Colors from "../../helpers/Colors";
import styles from "./styles";
import BookService from "../../services/BookService";
import Books from "../Books";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      items: []
    };
  }

  /** begin search */
  _searchNow = searchTerm => {
    this.setState({ searchTerm });
    if (searchTerm.length > 2) {
      BookService.search(searchTerm).then(items => {
        if (items) {
          this.setState({
            items
          });
        }
      });
    }
  };

  /** clear search and data */
  _clearSearch = () => {
    this.setState({
      items: [],
      searchTerm: ""
    });
  };

  render() {
    return (
      <Container>
        <Header
          iosBarStyle="light-content"
          searchBar
          rounded
          style={styles.header}
          androidStatusBarColor={Colors.blue}>
          <Item>
            <Icon name="search" />
            <Input
              placeholder="Search"
              placeholderTextColor="grey"
              value={this.state.searchTerm}
              onChangeText={searchTerm => this._searchNow(searchTerm)}
              onSubmitEditing={() => this._searchNow(this.state.searchTerm)}
            />
            <Icon name="close-circle" onPress={this._clearSearch} />
          </Item>
        </Header>
        {this.state.items.length == 0 ? (
          <View style={styles.search}>
            <H3 style={{ textAlign: "center" }}>Search for audiobooks</H3>
            <Text style={{ textAlign: "center", color: "grey" }}>
              {/* You can search by title, author, narrator & genre. */}
              You can search by title.
            </Text>
          </View>
        ) : (
          <Books data={this.state.items} navigation={this.props.navigation} />
        )}
      </Container>
    );
  }
}

export default Search;
