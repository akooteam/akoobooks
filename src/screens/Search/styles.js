import {
  StyleSheet
} from 'react-native';
import Colors from '../../helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    backgroundColor: Colors.blue
  },
  search: {
    marginTop: 40,
    alignSelf: 'center',
  }
})

export default styles