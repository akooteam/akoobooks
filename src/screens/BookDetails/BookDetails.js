import React, { Component } from "react";
import { Container, Button, Icon, Toast } from "native-base";
import Colors from "../../helpers/Colors";
import MainHeader from "../../components/MainHeader";
import BookContent from "../../components/BookContent";
import BookService from "../../services/BookService";

const Spinkit = require("react-native-spinkit");

class BookDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      refreshing: false,
      data: {}
    };
  }

  componentDidMount() {
    const { loading } = this.state;
    const { data } = this.props.navigation.state.params;

    BookService.getSingleBook(data.id)
      .then(res => {
        this.setState({
          loading: !loading,
          data: res
        });
      })
      .catch(e => {
        Toast.show({
          type: "danger",
          text: e.message,
          position: "top"
        });
      });
  }

  _onRefresh = () => {
    const { refreshing } = this.state;
    const { data } = this.props.navigation.state.params;

    this.setState({
      refreshing: !refreshing
    });

    BookService.getSingleBook(data.id)
      .then(res => {
        this.setState({
          refreshing: false,
          data: res
        });
      })
      .catch(e => {
        this.setState({
          refreshing: false
        });
        Toast.show({
          type: "danger",
          text: e.message,
          position: "top"
        });
      });
  };

  render() {
    const { navigation } = this.props;
    const { loading, data, refreshing } = this.state;

    return (
      <Container>
        <MainHeader
          left={
            <Button icon transparent onPress={() => navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "white" }} />
            </Button>
          }
          title={data.title}
        />
        {loading ? (
          <Spinkit
            type="ChasingDots"
            color={Colors.blue}
            style={{ alignSelf: "center", marginTop: 50 }}
          />
        ) : (
          <BookContent
            navigation={navigation}
            data={data}
            refreshing={refreshing}
            _onRefresh={this._onRefresh}
          />
        )}
      </Container>
    );
  }
}

export default BookDetails;
