import { StyleSheet } from "react-native";
import Colors from "../../helpers/Colors";

const styles = StyleSheet.create({
  container: {},
  rating: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10
  },
  label: {
    fontWeight: "normal",
    marginBottom: 5,
    marginTop: 20
  },
  button: { backgroundColor: Colors.green, marginVertical: 30 }
});

export default styles;
