import React, { Component } from "react";
import { View } from "react-native";
import {
  Container,
  Button,
  Icon,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Textarea,
  H3,
  Toast
} from "native-base";
import StarRating from "react-native-star-rating";
import MainHeader from "../../components/MainHeader";
import styles from "./styles";
import { colors } from "../../utils/themes";
import BookService from "../../services/BookService";
import { inject } from "mobx-react/native";
import UserService from "../../services/UserService";
import Spinner from "react-native-loading-spinner-overlay";
import Colors from "../../helpers/Colors";
import _ from "lodash";

const Spinkit = require("react-native-spinkit");

@inject("rootStore")
class Review extends Component {
  constructor(props) {
    super(props);

    const { id } = props.navigation.state.params.data;

    this.state = {
      id,
      rating: 1,
      review: undefined,
      loading: false
    };
  }

  componentDidMount() {
    let { reviews } = this.props.navigation.state.params.data;

    const { id } = this.props.rootStore.userStore.user;

    let obj = _.find(reviews, o => {
      return o.by.id == id;
    });

    if (obj) {
      this.setState({
        rating: obj.rating,
        review: obj.review
      });
    }
  }

  onStarRatingPress(rating) {
    this.setState({
      rating
    });
  }

  _handleReview = () => {
    const token = this.props.rootStore.userStore.userToken;

    UserService.setToken(token);

    const { goBack } = this.props.navigation;
    let { id, rating, review } = this.state;

    review = review.trim();

    if (review.length < 20) {
      Toast.show({
        type: "warning",
        text: "Review must be more than 20 characters",
        position: "top"
      });

      return;
    }

    this.setState({
      loading: true
    });

    BookService.submitReview({
      id,
      rating,
      review
    })
      .then(res => {
        this.setState({
          loading: false
        });

        if (res) {
          Toast.show({
            type: "success",
            text: "Thank you for your review",
            position: "top"
          });

          goBack();
          return;
        }

        throw 500;
      })
      .catch(error => {
        Toast.show({
          type: "danger",
          text: error.message,
          position: "top"
        });
      });
  };

  _renderLeft = () => (
    <Button transparent icon onPress={() => this.props.navigation.goBack()}>
      <Icon name="arrow-back" style={{ color: "white" }} />
    </Button>
  );

  render() {
    let { loading, rating, review } = this.state;
    let { data } = this.props.navigation.state.params;

    return (
      <Container style={styles.container}>
        <MainHeader left={this._renderLeft()} title={data.title} />
        <Content padder>
          <Spinner
            visible={loading}
            customIndicator={<Spinkit type="ChasingDots" color={Colors.blue} />}
          />
          <Text>Tap stars to rate</Text>
          <View>
            <View style={styles.rating}>
              <H3>Overall</H3>
              <StarRating
                disabled={false}
                maxStars={5}
                starSize={30}
                fullStarColor={colors.star}
                rating={rating}
                selectedStar={rating => this.onStarRatingPress(rating)}
              />
            </View>
          </View>
          <Form style={{ marginTop: 10 }}>
            <Label style={styles.label}>Let us Know Your Thoughts</Label>
            <Item regular>
              <Textarea
                rowSpan={5}
                placeholder="Write Your Review"
                style={{ fontSize: 17, padding: 10 }}
                value={review}
                onChangeText={review => this.setState({ review })}
              />
            </Item>
          </Form>
          <Button
            block
            rounded
            style={styles.button}
            onPress={this._handleReview}>
            <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Review;
