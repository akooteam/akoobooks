import React, { Component } from "react";
import { View, WebView, Platform, Alert } from "react-native";
import MainHeader from "../../components/MainHeader";
import { Container, Button, Icon, Text } from "native-base";

class WebViewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
	}
 
	render() {
    const { title, link } = this.props.navigation.state.params
    const { goBack } = this.props.navigation
    
		return (
      <Container>
				<MainHeader left={
            <Button transparent onPress={() => goBack()}>
              <Text style={{color: 'white'}}>Close</Text>
            </Button>
          } title={title} />
				<WebView source={{uri: link }} ref="webview" startInLoadingState={true} scrollEnabled={true}/>
      </Container>
    );
  }
}

export default WebViewScreen;
