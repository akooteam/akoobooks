import React, { Component } from "react";
import { View, Alert } from "react-native";
import { Container, Content, H3, Text, Header } from "native-base";
import { withNavigationFocus } from "react-navigation";
import MainHeader from "../../components/MainHeader";
import { observer, inject } from "mobx-react";
import Books from "../Books";
import FavoriteService from "../../services/FavoriteService";
import UserService from "../../services/UserService";
import Colors from "../../helpers/Colors";

@inject("rootStore")
@observer
class Favorites extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      // The screen is focused
      // Call any action
      this._fetchOverApi();
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }

  /** Make request to api */
  _fetchOverApi = page => {
    const { navigate } = this.props.navigation;
    const { favoriteStore, userStore } = this.props.rootStore;

    try {
      if (userStore.userToken) {
        this.setState({
          loading: true
        });
        UserService.setToken(userStore.userToken);

        FavoriteService.fetchFavorites().then(res => {
          this.setState({
            loading: false
          });
          favoriteStore.createFavorites(res);
        });
      }
    } catch (error) {
      Alert.alert(
        "Error",
        "Sign to view and manage your favorites",
        [
          { text: "OK", onPress: () => navigate("Account") },
          {
            text: "Cancel",
            style: "cancel"
          }
        ],
        { cancelable: false }
      );
    }
  };

  render() {
    const { loading } = this.state;
    const { allFavorites } = this.props.rootStore.favoriteStore;

    if (allFavorites.length == 0) {
      return (
        <Container>
          <MainHeader title="Favorites" />
          <View style={{ marginTop: 40, alignItems: "center" }}>
            <View>
              <H3 style={{ marginBottom: 10, textAlign: "center" }}>
                Your favorites is empty
              </H3>
              <Text style={{ textAlign: "center" }}>
                Browse to find something to add to your favorites
              </Text>
            </View>
          </View>
        </Container>
      );
    }

    return (
      <Container>
        <MainHeader title="Favorites" />
        <Books
          navigation={this.props.navigation}
          data={allFavorites}
          refreshing={loading}
          _onRefresh={this._fetchOverApi}
        />
      </Container>
    );
  }
}

export default withNavigationFocus(Favorites);
