import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  InteractionManager
} from "react-native";
import { Text, Card, CardItem, Right, Icon, ActionSheet } from "native-base";
import { FlatGrid } from "react-native-super-grid";
import { observer, inject } from "mobx-react/native";

const BUTTONS = ["View Info", "Cancel"];
var DESTRUCTIVE_INDEX = 1;
var CANCEL_INDEX = 1;

@observer
class Books extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        loading: false
      });
    });
  }

  _toggleActionSheet = item => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX
      },
      buttonIndex => {
        this._optionPress(buttonIndex, item);
        // this.setState({ clicked: BUTTONS[buttonIndex] });
      }
    );
  };

  _optionPress = (key, item) => {
    const { navigate } = this.props.navigation;

    switch (key) {
      case 0:
        navigate("BookDetails", { data: item });
        break;

      default:
        break;
    }
  };

  render() {
    const { props } = this;

    if (this.state.loading) {
      return <View />;
    }

    return (
      <FlatGrid
        refreshing={props.refreshing}
        onRefresh={props._onRefresh}
        itemDimension={130}
        items={props.data}
        renderItem={({ item, index }) => (
          <Card>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() =>
                props.navigation.navigate("BookDetails", {
                  data: item
                })
              }>
              <CardItem cardBody>
                <Image
                  source={{
                    uri: item.cover_image
                  }}
                  style={{ height: 200, width: null, flex: 1 }}
                />
              </CardItem>
            </TouchableOpacity>
            <CardItem footer style={{ justifyContent: "space-between" }}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() =>
                  props.navigation.navigate("BookDetails", { data: item })
                }
                style={{ flexDirection: "column", width: "80%" }}>
                <Text numberOfLines={1} style={{ fontSize: 14 }}>
                  {item.title}
                </Text>
                <Text
                  numberOfLines={1}
                  style={{ fontSize: 12, fontWeight: "300", color: "grey" }}>
                  {item.authors[0].name}
                </Text>
              </TouchableOpacity>
              <Right>
                <TouchableOpacity
                  onPress={() => this._toggleActionSheet(item)}
                  style={{
                    width: 30,
                    alignItems: "flex-end"
                  }}>
                  <Icon name="more" />
                </TouchableOpacity>
              </Right>
            </CardItem>
          </Card>
        )}
      />
    );
  }
}

export default Books;
