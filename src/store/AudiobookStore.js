import { types } from "mobx-state-tree";
import { Audiobook } from "./models/Audiobook";
import _ from "lodash";

export const AudiobookStore = types
  .model("AudiobookStore", {
    identifier: types.optional(types.identifier, "AudiobookStore"),
    audiobooks: types.array(Audiobook)
  })
  .views(self => ({
    get count() {
      return self.audiobooks.length;
    },
    bookExist(audiobook) {
      if (_.find(self.audiobooks, { id: audiobook.id })) {
        return true;
      }
      return false;
    }
  }))
  .actions(self => ({
    createAudiobook(audiobooks) {
      self.audiobooks = audiobooks;

      return audiobooks;
    }
    // removeFavorite(index) {
    //   self.favorites.splice(index, -1);
    // }
  }));
