import { types } from "mobx-state-tree";
import { Audiobook } from "./models/Audiobook";
import _ from "lodash";

export const SubscriptionStore = types
  .model("SubscriptionStore", {
    identifier: types.optional(types.identifier, "SubscriptionStore"),
    subscriptions: types.array(Audiobook)
  })
  .views(self => ({
    get count() {
      return self.subscriptions.length;
    },
    bookExist(audiobook) {
      if (_.find(self.subscriptions, { id: audiobook.id })) {
        return true;
      }
      return false;
    }
  }))
  .actions(self => ({
    createAudiobook(subscriptions) {
      self.subscriptions = subscriptions;
      // self.subscriptions = [];

      return subscriptions;
    }
  }));
