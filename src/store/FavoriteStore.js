import { types } from "mobx-state-tree";
import { autorun } from "mobx";
import { Favorite } from "./models/Favorite";
import _ from "lodash";

export const FavoriteStore = types
  .model("FavoriteStore", {
    identifier: types.optional(types.identifier, "FavoriteStore"),
    favorites: types.array(Favorite)
  })
  .views(self => ({
    get allFavorites() {
      return self.favorites;
    },
    get count() {
      return self.favorites.length;
    },
    favoriteExist(favorite) {
      if (_.find(self.favorites, { id: favorite.id })) {
        return true;
      }
      return false;
    }
  }))
  .actions(self => ({
    createFavorites(favorites) {
      self.favorites = favorites;

      return favorites;
    }
  }));
