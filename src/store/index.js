import React from "react";
import { AsyncStorage, Platform } from "react-native";
import { types, onSnapshot, getSnapshot, getRoot } from "mobx-state-tree";
import { UserStore } from "./UserStore";
import { PlayerStore } from "./PlayerStore";
import { FavoriteStore } from "./FavoriteStore";
import { AudiobookStore } from "./AudiobookStore";
import { SubscriptionStore } from "./SubscriptionStore";

export const RootStore = types
  .model("RootStore", {
    identifier: types.optional(types.identifier, "RootStore"),
    userStore: types.optional(UserStore, () =>
      UserStore.create({
        isAuthenticated: false
      })
    ),
    playerStore: types.optional(PlayerStore, () =>
      PlayerStore.create({
        playbackState: Platform.OS == "android" ? 0 : "",
        speed: 1,
        modal: false,
        timer: 0,
        showMiniPlayer: false
      })
    ),
    favoriteStore: types.optional(FavoriteStore, () =>
      FavoriteStore.create({
        identifier: "favorites_store",
        favorites: []
      })
    ),
    audiobookStore: types.optional(AudiobookStore, () =>
      AudiobookStore.create({
        identifier: "audiobook_id",
        audiobooks: []
      })
    ),
    subscriptionStore: types.optional(SubscriptionStore, () =>
      SubscriptionStore.create({
        identifier: "subscription_id",
        subscriptions: []
      })
    )
  })
  .views(self => ({
    get subscriptionCount() {
      if (self.userStore.user && self.userStore.user.subscription_profile) {
        return (
          self.userStore.user.subscription_profile.count -
          self.subscriptionStore.count
        );
      }
      return 0;
    }
  }))
  .actions(self => ({
    async save() {
      try {
        const transformedSnapshot = getSnapshot(self);
        const json = JSON.stringify(transformedSnapshot);

        await AsyncStorage.setItem("appStatePersistenceKey", json);
      } catch (err) {
        console.warn("unexpected error " + err);
      }
    },
    afterCreate() {
      onSnapshot(self, () => {
        const rootStore = getRoot(self);
        rootStore.save();
      });
    }
  }));
