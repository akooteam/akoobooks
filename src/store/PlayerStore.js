import { types } from "mobx-state-tree";
import { Track, CurrentTrack } from "./models/Player";
import { Platform } from "react-native";
import _ from "lodash";

export const PlayerStore = types
  .model("PlayerStore", {
    identifier: types.optional(types.identifier, "PlayerStore"),
    playbackState: Platform.OS == "android" ? types.integer : types.string,
    track: types.maybeNull(Track),
    currentTrack: types.maybe(CurrentTrack),
    speed: types.number,
    modal: types.boolean,
    timer: types.number,
    showMiniPlayer: types.optional(types.boolean, false),
    duration: types.maybeNull(types.number)
  })
  .views(self => ({
    get getPlaylist() {
      if (self.currentTrack) {
        return self.currentTrack.playlist;
      }
    }
  }))
  .actions(self => ({
    setTrack(json) {
      const track = Track.create({
        ...json
      });

      self.track = track;

      return track;
    },
    setCurrentBook(json) {
      self.currentTrack = json;
    },
    setPlayback(state) {
      self.playbackState = state;
    },
    setSpeed() {
      switch (self.speed) {
        case 1.0:
          self.speed = 1.25;
          break;

        case 1.25:
          self.speed = 1.5;
          break;

        case 1.5:
          self.speed = 2.0;
          break;

        default:
          self.speed = 1.0;
          break;
      }
    },
    toggleModal() {
      self.modal = !self.modal;
    },
    setTimer(time) {
      self.timer = time;
    },
    showPlayer(state) {
      self.showMiniPlayer = state;
    },
    clearTrack() {
      self.showMiniPlayer = false;
      self.track = null;
      self.duration = null;
    },
    setDuration(duration) {
      self.duration = duration;
    }
  }));
