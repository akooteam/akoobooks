import { types } from "mobx-state-tree";
import { User } from "./models/User";

export const UserStore = types
  .model("UserStore", {
    identifier: types.optional(types.identifier, "UserStore"),
    user: types.maybe(User),
    isAuthenticated: types.boolean
  })
  .views(self => ({
    get userToken() {
      return self.user.token;
    },
    get getMembershipPlan() {
      if (self.user && self.user.subscription_profile) {
        return self.user.subscription_profile.title;
      }

      return "none";
    }
  }))
  .actions(self => ({
    createUser(userJson) {
      const user = User.create({
        ...userJson
      });
      self.user = user;

      return user;
    },
    toggleAuthenticated(value) {
      self.isAuthenticated = value;
    },
    updateUser(user) {
      self.user = user;
    },
    logout() {
      self.isAuthenticated = false;
      self.user = undefined;
    }
  }));
