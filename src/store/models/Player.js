import { types } from "mobx-state-tree";

export const Track = types.model("Track", {
  id: types.maybe(types.identifier),
  title: types.maybe(types.string),
  artist: types.maybe(types.string),
  artwork: types.maybe(types.string),
  url: types.maybe(types.string),
  purchased: types.maybeNull(types.boolean),
  order: types.maybe(types.integer)
});

export const CurrentTrack = types.model("CurrentTrack", {
  id: types.identifier,
  title: types.string,
  artist: types.string,
  artwork: types.string,
  playlist: types.frozen()
});
