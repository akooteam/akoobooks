import { types } from "mobx-state-tree";

export const User = types.model("User", {
  id: types.integer,
  first_name: types.string,
  last_name: types.string,
  email: types.string,
  phone_number: types.maybeNull(types.string),
  token: types.string,
  photo_url: types.maybeNull(types.string),
  is_active: types.boolean,
  created_at: types.maybeNull(types.string),
  subscription_profile: types.maybe(types.frozen())
});
