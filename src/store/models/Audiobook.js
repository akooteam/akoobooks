import { types } from "mobx-state-tree";

export const Audiobook = types.model("Audiobook", {
  id: types.maybe(types.integer),
  title: types.maybe(types.string),
  description: types.maybe(types.string),
  cover_image: types.maybe(types.string),
  sample_audio: types.maybe(types.string),
  language: types.maybe(types.frozen()),
  genres: types.maybe(types.frozen()),
  authors: types.maybe(types.frozen()),
  narrators: types.maybe(types.frozen()),
  chapters: types.maybe(types.frozen()),
  is_active: types.maybe(types.boolean),
  is_published: types.maybe(types.boolean),
  created_at: types.maybe(types.string)
});
