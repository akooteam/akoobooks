import React from "react";
import { FlatList, View } from "react-native";
import { List, Text, ListItem, Left, Thumbnail, Body } from "native-base";

const renderItem = (item, i, items) => {
  return (
    <View
      style={{
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 10,
        borderBottomWidth: items.length - 1 != i ? 0.5 : 0,
        borderBottomColor: "lightgrey",
        width: "90%"
      }}>
      <Thumbnail
        source={{ uri: item.author_image }}
        style={{ marginBottom: 10 }}
      />
      <View style={{ marginLeft: 10, marginBottom: 10, width: "90%" }}>
        <Text>{item.name}</Text>
        <Text note style={{ marginTop: 5 }}>
          {item.about}
        </Text>
      </View>
    </View>
  );
};

const AuthorList = props => {
  return (
    <FlatList
      data={props.list}
      renderItem={({ item, index }) => renderItem(item, index, props.list)}
      keyExtractor={item => `${item.id}`}
    />
  );
};

export default AuthorList;
