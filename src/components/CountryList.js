import React, { Component } from "react";
import { View } from "react-native";
import {
  Container,
  Button,
  Text,
  Icon,
  Content,
  ListItem,
  Left,
  Item,
  Input,
  Right
} from "native-base";
import MainHeader from "./MainHeader";
import { FlatList } from "react-native-gesture-handler";
import _ from "lodash";

const countryData = require("../assets/country_dial_info");

class CountryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: countryData,
      searchTerm: ""
    };
  }

  _setCountry = item => {
    const { setCountry, toggleModal } = this.props;
    setCountry(item);
    toggleModal();
  };

  _renderItem = ({ item }) => (
    <ListItem onPress={() => this._setCountry(item)}>
      <Left>
        <Text>{item.flag}</Text>
        <Text style={{ marginLeft: 5 }}>{item.name}</Text>
      </Left>
      <Right>
        <Text note>{item.dial_code}</Text>
      </Right>
    </ListItem>
  );

  _searchList = searchTerm => {
    const country = countryData.filter(item => {
      const itemName = _.toLower(item.name);

      const textName = _.toLower(searchTerm);

      return itemName.indexOf(textName) > -1;
    });
    this.setState({ searchTerm, country });
  };

  render() {
    const { toggleModal } = this.props;
    const { country } = this.state;
    return (
      <Container>
        <MainHeader
          title="Choose your country code"
          style={{ flex: 3 }}
          left={
            <Button transparent onPress={toggleModal}>
              <Icon name="arrow-back" style={{ color: "white" }} />
            </Button>
          }
        />
        <View style={{ backgroundColor: "lightgrey", paddingHorizontal: 10 }}>
          <Item>
            <Icon name="search" />
            <Input
              placeholder="Search by country name"
              value={this.state.searchTerm}
              onChangeText={searchTerm => this._searchList(searchTerm)}
            />
          </Item>
        </View>
        <Content>
          <FlatList
            data={country}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => item.code}
          />
        </Content>
      </Container>
    );
  }
}

export default CountryList;
