import React, { Component } from "react";
import {
  View,
  Alert,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Text as RNText,
  RefreshControl
} from "react-native";
import { Content, Text, Button, Icon, H3 } from "native-base";
import SectionHeader from "./SectionHeader";
import PurchasedButton from "./PurchasedButton";
import SubscriptionButton from "./SubscriptionButton";
import FavoriteButton from "./FavoriteButton";
import AuthorList from "./AuthorList";
import TrackPlayer from "react-native-track-player";
import Layout from "../helpers/Layout";
import Colors from "../helpers/Colors";
import Reviews from "./Reviews";
import { metrics } from "../utils/themes";
import { observer, inject } from "mobx-react";
import _ from "lodash";
import PlayButton from "./PlayButton";
import CollapsedAuthorList from "./CollapsedAuthorList";

@inject("rootStore")
@observer
class BookContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: true,
      authorCollapsed: true,
      authorBioCollapsed: true,
      narratorsCollapsed: true
    };
  }

  _playSampleAudio = async () => {
    const { data } = this.props;
    const { showPlayer, setTrack } = this.props.rootStore.playerStore;

    await TrackPlayer.reset();

    let payload = {
      id: `${data.id}`,
      title: data.title,
      artist: data.authors[0].name,
      artwork: data.cover_image,
      url: data.sample_audio
    };

    await TrackPlayer.add(payload);

    showPlayer(true);
    await setTrack(payload);

    await TrackPlayer.play();
  };

  _navigateToReviews = () => {
    let { data } = this.props;
    const { navigate } = this.props.navigation;
    const { userStore } = this.props.rootStore;

    if (!userStore.user) {
      Alert.alert("Error", "Please login to continue");
      return;
    }

    navigate("Reviews", { data: data });
  };

  _toggleNarrators = () => {
    this.setState({
      narratorsCollapsed: !this.state.narratorsCollapsed
    });
  };

  _toggleAuthorsBio = () => {
    this.setState({
      authorBioCollapsed: !this.state.authorBioCollapsed
    });
  };

  static defaultProps = {
    options: true
  };

  render() {
    const { data, _onRefresh, refreshing, options } = this.props;
    const { bookExist } = this.props.rootStore.audiobookStore;
    const { navigate } = this.props.navigation;
    const {
      collapsed,
      authorCollapsed,
      authorBioCollapsed,
      narratorsCollapsed
    } = this.state;

    return (
      <Content
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }>
        <ImageBackground
          source={{
            uri: data.cover_image
          }}
          style={{
            width: "100%",
            resizeMode: "contain",
            height: Layout.window.width * 0.7
          }}>
          <View
            style={{
              justifyContent: "flex-end",
              height: Layout.window.width * 0.7,
              ...StyleSheet.absoluteFillObject,
              backgroundColor: "rgba(0,0,0,0.3)"
            }}>
            {options && (
              <Button
                icon
                transparent
                large
                style={{
                  alignSelf: "flex-end"
                }}
                onPress={this._playSampleAudio}>
                <Icon
                  type="MaterialIcons"
                  name="play-circle-filled"
                  style={{
                    fontSize: 40,
                    color: Colors.white
                  }}
                />
              </Button>
            )}
          </View>
        </ImageBackground>
        <View
          style={{
            paddingHorizontal: 16
          }}>
          <View style={{ alignItems: "center", marginTop: 20 }}>
            <H3 style={{ textAlign: "center" }}>{data.title}</H3>
            {authorCollapsed && data.authors.length > 3 ? (
              <View style={{ marginVertical: 5 }}>
                {data.authors.map((o, i) => {
                  if (i < 3) {
                    return (
                      <Text
                        key={o.id}
                        style={{
                          textAlign: "center",
                          marginVertical: 1,
                          color: Colors.grey
                        }}>
                        {o.name}
                      </Text>
                    );
                  }
                })}
                <TouchableOpacity
                  style={{ marginTop: 5 }}
                  onPress={() => this.setState({ authorCollapsed: false })}>
                  <Text
                    style={{
                      color: Colors.buttonBlue,
                      textAlign: "center",
                      fontSize: 14,
                      textTransform: "lowercase"
                    }}>
                    More...
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ marginVertical: 5 }}>
                {data.authors.map(o => (
                  <Text
                    key={o.id}
                    style={{
                      textAlign: "center",
                      marginVertical: 1,
                      color: Colors.grey
                    }}>
                    {o.name}
                  </Text>
                ))}
                {data.authors.length > 3 && (
                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                    onPress={() => this.setState({ authorCollapsed: true })}>
                    <Text
                      style={{
                        color: Colors.buttonBlue,
                        textAlign: "center",
                        fontSize: 14,
                        textTransform: "lowercase"
                      }}>
                      less...
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            )}
          </View>
          {options && (
            <View>
              <PlayButton {...this.props} />
              <SubscriptionButton {...this.props} />
              <PurchasedButton {...this.props} />
              <FavoriteButton {...this.props} />
            </View>
          )}
          <SectionHeader title="Summary" />
          {collapsed ? (
            <View>
              <Text numberOfLines={3}>{data.description}</Text>
              <TouchableOpacity
                style={{ marginTop: 5 }}
                onPress={() => this.setState({ collapsed: false })}>
                <Text
                  style={{
                    color: Colors.buttonBlue,
                    fontSize: 14,
                    textTransform: "lowercase"
                  }}>
                  Read More
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <Text>{data.description}</Text>
          )}
          <SectionHeader
            title="Reviews"
            right={
              <TouchableOpacity onPress={this._navigateToReviews}>
                <Text style={{ color: Colors.buttonBlue }}>Write Review</Text>
              </TouchableOpacity>
            }
          />
          <Reviews reviews={data.reviews} />
          <SectionHeader title="Authors" />
          {authorBioCollapsed && data.authors.length > 3 ? (
            <CollapsedAuthorList
              list={data.authors}
              collapsed={this._toggleAuthorsBio}
            />
          ) : (
            <AuthorList list={data.authors} />
          )}
          <SectionHeader title="Narrators" />
          {narratorsCollapsed && data.authors.length > 3 ? (
            <CollapsedAuthorList
              list={data.narrators}
              collapsed={this._toggleNarrators}
            />
          ) : (
            <AuthorList list={data.narrators} />
          )}
          <SectionHeader title="Info" />
          <View style={{ marginBottom: 20 }}>
            <Text>
              <Text style={{ fontWeight: "bold" }}>Genres: </Text>
              {data.genres.map((o, i) => (
                <Text key={o.id}>{i == 0 ? o.name : `, ${o.name}`}</Text>
              ))}
            </Text>
            <Text>
              <Text style={{ fontWeight: "bold" }}>Language:</Text>{" "}
              <Text>{data.language.name}</Text>
            </Text>
          </View>
        </View>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  cover: {
    marginTop: 60,
    justifyContent: "flex-end",
    alignSelf: "center",
    width: Layout.window.width * 0.7,
    height: Layout.window.width * 0.7
  }
});

export default BookContent;
