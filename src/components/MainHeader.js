import React from "react";
import { StyleSheet } from "react-native";
import { Header, Body, Title, Left, Right } from "native-base";
import Colors from "../helpers/Colors";

const MainHeader = props => {
  return (
    <Header
      iosBarStyle="light-content"
      style={styles.header}
      hasSegment={props.hasSegment ? true : false}
      androidStatusBarColor={Colors.blue}
      hasTabs={props.hasTabs ? true : false}>
      <Left>{props.left}</Left>
      <Body style={{ ...props.style }}>
        <Title style={{ color: "white" }}>{props.title}</Title>
      </Body>
      <Right>{props.right}</Right>
    </Header>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.blue
  }
});

export default MainHeader;
