import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { Button, Text, Icon } from "native-base";
import Colors from "../helpers/Colors";
import TrackPlayer from "react-native-track-player";
import { observer, inject } from "mobx-react";
import formatter from "../utils/formatter";
import _ from "lodash";

export default class PlayButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _mutatePayload = () => {
    const { data } = this.props;

    let playlist = _.map(data.chapters, o => {
      return {
        id: `${o.id}`,
        title: o.title,
        artist: data.authors[0].name,
        artwork: data.cover_image,
        url: o.audio_stream_url,
        order: o.order,
        purchased: true,
        duration: formatter.duration(o.duration)
      };
    });

    playlist = _.sortBy(playlist, "order");

    return {
      id: `${data.id}`,
      title: data.title,
      artwork: data.cover_image,
      artist: data.authors[0].name,
      playlist
    };
  };

  _playBook = async () => {
    const { data } = this.props;
    const { track } = this.props.rootStore.playerStore;
    const { navigate } = this.props.navigation;

    const {
      showPlayer,
      setTrack,
      setCurrentBook
    } = this.props.rootStore.playerStore;

    let tracks = this._mutatePayload();

    await setCurrentBook(tracks);

    await TrackPlayer.reset();

    await TrackPlayer.add(tracks.playlist);

    let loadedTrack = track && _.find(tracks.playlist, { id: track.id });

    if (loadedTrack) {
      await setTrack(loadedTrack);
    } else {
      await setTrack(tracks.playlist[0]);
    }

    showPlayer(true);

    navigate("Player");
  };

  render() {
    const { audiobookStore, subscriptionStore } = this.props.rootStore;
    const { data } = this.props;

    if (audiobookStore.bookExist(data) || subscriptionStore.bookExist(data)) {
      return (
        <Button
          icon
          block
          rounded
          style={styles.button}
          onPress={this._playBook}>
          <Icon name="play" />
          <Text>Play</Text>
        </Button>
      );
    }

    return <View />;
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
    backgroundColor: Colors.buttonBlue
  }
});
