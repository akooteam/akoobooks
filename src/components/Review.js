/**
 * @format
 * @flow
 */
import React, { PureComponent } from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { colors, metrics } from "../utils/themes";
import StarRating from "react-native-star-rating";
import moment from "moment";

class Review extends PureComponent {
  render() {
    const { width, item } = this.props;
    return (
      <View
        style={[
          styles.container,
          width && {
            width: width
          },
          { marginRight: 5 }
        ]}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={{ fontSize: 17, marginRight: 5 }}>{`${
            item.by.first_name
          } ${item.by.last_name}`}</Text>
          <StarRating
            disabled={true}
            starSize={15}
            maxStars={5}
            rating={item.rating}
            fullStarColor={colors.star}
          />
        </View>
        <Text style={{ fontSize: 16, marginTop: 5 }}>{item.review}</Text>
        <View style={styles.author}>
          <Text>{moment(item.created_at).format("MMM DD")}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    padding: metrics.padding,
    marginBottom: metrics.padding,
    borderRadius: metrics.radius
  },
  author: {
    position: "absolute",
    right: metrics.padding,
    top: metrics.padding
  },
  subText: {
    fontSize: 13,
    fontWeight: "normal",
    color: "#8D8D92"
  }
});

export default Review;
