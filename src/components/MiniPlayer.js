import React, { Component } from "react";
import {
  Platform,
  Image,
  TouchableHighlight,
  View,
  StyleSheet
} from "react-native";
import SplashScreen from "react-native-splash-screen";
import TrackPlayer, { ProgressComponent } from "react-native-track-player";
import { Button, Icon, Text } from "native-base";
import Colors from "../helpers/Colors";
import { observer, inject } from "mobx-react/native";

@inject("rootStore")
@observer
class MiniPlayer extends ProgressComponent {
  constructor(props) {
    super(props);

    this.state = {
      play: ""
    };
  }

  async componentWillMount() {
    const {
      track,
      clearTrack,
      duration,
      getPlaylist
    } = this.props.rootStore.playerStore;

    TrackPlayer.setupPlayer();
    TrackPlayer.updateOptions({
      stopWithApp: true,
      jumpInterval: 30,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_STOP,
        TrackPlayer.CAPABILITY_JUMP_BACKWARD,
        TrackPlayer.CAPABILITY_JUMP_FORWARD
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_STOP,
        TrackPlayer.CAPABILITY_JUMP_BACKWARD,
        TrackPlayer.CAPABILITY_JUMP_FORWARD
      ],
      notificationCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_STOP,
        TrackPlayer.CAPABILITY_JUMP_BACKWARD,
        TrackPlayer.CAPABILITY_JUMP_FORWARD
      ]
    });

    if (Platform.OS == "ios") {
      this.props.rootStore.playerStore.setPlayback("paused");
    }

    try {
      if (!track.purchased) {
        clearTrack();
      } else {
        const currentTrack = await TrackPlayer.getCurrentTrack();
        if (currentTrack == null) {
          await TrackPlayer.add(getPlaylist);
          await TrackPlayer.skip(track.id);
          await TrackPlayer.seekTo(duration);
        }
      }
    } catch (_) {}

    SplashScreen.hide();
  }

  _togglePlay = async () => {
    const { speed, playbackState } = this.props.rootStore.playerStore;

    if (playbackState == TrackPlayer.STATE_PLAYING) {
      await TrackPlayer.pause();
    } else {
      await TrackPlayer.play();
      TrackPlayer.setRate(speed);
    }
  };

  render() {
    const {
      playbackState,
      showMiniPlayer,
      track,
      currentTrack
    } = this.props.rootStore.playerStore;
    const { navigate } = this.props.navigation;

    if (!track) {
      return <View />;
    }

    return (
      <TouchableHighlight onPress={() => track.purchased && navigate("Player")}>
        <View style={styles.player}>
          <View
            style={[{ width: this.getProgress() * 100 + "%" }, styles.bar]}
          />
          <View style={styles.row}>
            <View style={{ width: "20%" }}>
              <Image
                source={{
                  uri: track.artwork
                }}
                style={{
                  width: 60,
                  height: 60
                }}
              />
            </View>
            <View
              style={{
                alignSelf: "center",
                alignItems: "center",
                width: "60%"
              }}>
              <Text numberOfLines={1} style={{ textAlign: "center" }}>
                {track.purchased ? currentTrack.title : track.title}
              </Text>
              <Text note>{track.purchased ? track.title : "Sample Audio"}</Text>
            </View>
            <View style={{ width: "20%" }}>
              <Button
                icon
                transparent
                onPress={this._togglePlay}
                style={{ alignSelf: "flex-end" }}>
                <Icon
                  type="FontAwesome"
                  name={Platform.select({
                    ios:
                      playbackState == TrackPlayer.STATE_PLAYING
                        ? "pause"
                        : "play",
                    android:
                      playbackState == TrackPlayer.STATE_PLAYING ||
                      playbackState == 1
                        ? "pause"
                        : "play"
                  })}
                  style={{ color: "grey" }}
                />
              </Button>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  player: {
    elevation: 5,
    backgroundColor: Colors.white,
    minHeight: 60
  },
  row: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  bar: {
    height: 2,
    backgroundColor: Colors.yellow
  }
});

export default MiniPlayer;
