import React, { Component } from "react";
import { View } from "react-native";
import { H3, Text } from "native-base";

export default class EmptyState extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { title, subtext } = this.props;
    return (
      <View style={{ marginTop: 40, alignItems: "center" }}>
        <View>
          <H3 style={{ marginBottom: 10, textAlign: "center" }}>{title}</H3>
          <Text>{subtext}</Text>
        </View>
      </View>
    );
  }
}
