import React, { Component } from "react";
import { View, Text } from "react-native";
import UserService from "../services/UserService";
import { observer, inject } from "mobx-react";
import { Toast, Button, Icon } from "native-base";
import MainHeader from "./MainHeader";

@inject("rootStore")
class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.unsubscriber = null;
  }

  componentWillMount() {
    const { user } = this.props.rootStore.userStore;

    this.props.navigation.navigate(user ? "Profile" : "Auth");
  }

  render() {
    return (
      <View>
        <MainHeader title="Account" />
      </View>
    );
  }
}

export default Loading;
