import React, { PureComponent } from "react";
import { View } from "react-native";
import { FlatList } from "react-native";
import Review from "./Review";
import { metrics } from "../utils/themes";
import { Text, Icon } from "native-base";

class Reviews extends PureComponent {
  const;
  renderItem({ item }) {
    return (
      <Review
        key={item.id}
        item={item}
        width={metrics.screenWidth - metrics.lessPadding * 4}
        height={170}
      />
    );
  }

  render() {
    const { reviews } = this.props;

    if (reviews.length === 0) {
      return (
        <View>
          <Text style={{ marginBottom: 10, textAlign: "center" }}>
            No reviews. Be the first.
          </Text>
        </View>
      );
    }
    return (
      <View>
        <FlatList
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          data={reviews}
          renderItem={this.renderItem}
          keyExtractor={item => `${item.id}`}
        />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}>
          <Icon name="arrow-back" style={{ color: "grey" }} />
          <Text note> Swipe left or right read reviews</Text>
          <Icon name="arrow-forward" style={{ color: "grey" }} />
        </View>
      </View>
    );
  }
}

export default Reviews;
