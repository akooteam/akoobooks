import React, { Component } from "react";
import { View, StyleSheet, Linking, Alert } from "react-native";
import { Button, Text, Spinner } from "native-base";
import TrackPlayer from "react-native-track-player";
import { observer, inject } from "mobx-react";
import Colors from "../helpers/Colors";
import _ from "lodash";
import SubscriptionService from "../services/SubscriptionService";
import UserService from "../services/UserService";
import formatter from "../utils/formatter";

@inject("rootStore")
@observer
class SubscriptionButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  _mutatePayload = () => {
    const { data } = this.props;

    let playlist = _.map(data.chapters, o => {
      return {
        id: `${o.id}`,
        title: o.title,
        artist: data.authors[0].name,
        artwork: data.cover_image,
        url: o.audio_stream_url,
        order: o.order,
        purchased: true,
        duration: formatter.duration(o.duration)
      };
    });

    playlist = _.sortBy(playlist, "order");

    return {
      id: `${data.id}`,
      title: data.title,
      artwork: data.cover_image,
      artist: data.authors[0].name,
      playlist
    };
  };

  _playBook = async () => {
    const { data } = this.props;
    const { track } = this.props.rootStore.playerStore;
    const { navigate } = this.props.navigation;

    const {
      showPlayer,
      setTrack,
      setCurrentBook
    } = this.props.rootStore.playerStore;

    let tracks = this._mutatePayload();

    await setCurrentBook(tracks);

    await TrackPlayer.reset();

    await TrackPlayer.add(tracks.playlist);

    let loadedTrack = track && _.find(tracks.playlist, { id: track.id });

    if (loadedTrack) {
      await setTrack(loadedTrack);
    } else {
      await setTrack(tracks.playlist[0]);
    }

    showPlayer(true);

    navigate("Player");
  };

  _addToSubscription = () => {
    const { user } = this.props.rootStore.userStore;
    const { data } = this.props;
    const { navigate } = this.props.navigation;

    if (!user) {
      Alert.alert("Error", "Login to continue");
      return;
    }

    this.setState({
      loading: true
    });

    UserService.setToken(user.token);

    SubscriptionService.addBook(data.id).then(res => {
      this.setState({
        loading: false
      });

      if (!res)
        return Alert.alert("Error", "Unable to add book to your subscription");

      Alert.alert(
        "Success",
        "Audiobook added to your subscriptions",
        [{ text: "OK", onPress: () => navigate("Library") }],
        { cancelable: false }
      );
    });
  };

  render() {
    const { bookExist } = this.props.rootStore.subscriptionStore;
    const { navigate } = this.props.navigation;
    const { data } = this.props;

    if (bookExist(data)) {
      return <View />;
    }

    return (
      <View>
        <Button
          block
          rounded
          style={styles.button}
          onPress={this._addToSubscription}>
          {this.state.loading ? (
            <Spinner color={Colors.blue} />
          ) : (
            <Text>Add to Subscription</Text>
          )}
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
    backgroundColor: Colors.lightBlue
  }
});

export default SubscriptionButton;
