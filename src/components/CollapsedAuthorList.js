import React, { Component } from "react";
import { View, FlatList, TouchableOpacity } from "react-native";
import { Text, Thumbnail } from "native-base";
import Colors from "../helpers/Colors";

const renderItem = (item, i, items) => {
  if (i < 3) {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginBottom: 10,
          borderBottomWidth: i < 2 ? 0.5 : 0,
          borderBottomColor: "lightgrey",
          width: "90%"
        }}>
        <Thumbnail
          source={{ uri: item.author_image }}
          style={{ marginBottom: 10 }}
        />
        <View style={{ marginLeft: 10, marginBottom: 10, width: "90%" }}>
          <Text>{item.name}</Text>
          <Text note style={{ marginTop: 5 }}>
            {item.about}
          </Text>
        </View>
      </View>
    );
  }
};

const CollapsedAuthorList = props => {
  return (
    <View>
      <FlatList
        data={props.list}
        renderItem={({ item, index }) => renderItem(item, index, props.list)}
        keyExtractor={item => `${item.id}`}
      />
      {props.list.length > 3 && (
        <TouchableOpacity style={{ marginTop: 0 }} onPress={props.collapsed}>
          <Text
            style={{
              color: Colors.buttonBlue,
              fontSize: 14,
              textTransform: "lowercase"
            }}>
            More...
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CollapsedAuthorList;
