import React, { Component } from "react";
import { View, Alert } from "react-native";
import { observer, inject } from "mobx-react";
import { Text, Icon, Button, Toast } from "native-base";
import FavoriteService from "../services/FavoriteService";
import UserService from "../services/UserService";

@observer
class FavoriteButton extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _addToFavorite = () => {
    const { data } = this.props;

    FavoriteService.addFavorites(data.id).then(res => {
      if (res) {
        Toast.show({
          type: "success",
          text: "Added to favorites"
        });
        return;
      }
      Toast.show({
        type: "danger",
        text: "Add to Favorites failed"
      });
    });
  };

  _removeFavorite = () => {
    const { data } = this.props;
    const { navigate } = this.props.navigation;
    const { userStore, favoriteStore } = this.props.rootStore;

    try {
      if (userStore.user) {
        UserService.setToken(userStore.userToken);

        FavoriteService.removeFavorites(data.id).then(res => {
          console.log("res>>>>>>>>>>>", res);
          if (res) {
            favoriteStore.createFavorites(res);

            Toast.show({
              type: "success",
              text: "Book removed from favorites"
            });
            return;
          }

          Toast.show({
            type: "danger",
            text: "Unable to remove book from favorites"
          });
        });
      }
    } catch (e) {
      console.log("error>>>>>>>>>>>>", e);

      Alert.alert(
        "Error",
        "Sign to view and manage your favorites",
        [{ text: "OK", onPress: () => navigate("Account") }],
        { cancelable: false }
      );
    }
  };

  render() {
    const { data } = this.props;
    const { favoriteExist } = this.props.rootStore.favoriteStore;

    if (favoriteExist(data)) {
      return (
        <Button
          transparent
          icon
          block
          style={{ marginTop: 10 }}
          onPress={this._removeFavorite}>
          <Icon name="trash" style={{ color: "red" }} />
          <Text style={{ color: "red" }}> Remove from Favorites</Text>
        </Button>
      );
    }
    return (
      <Button
        transparent
        icon
        block
        style={{ marginTop: 10 }}
        onPress={this._addToFavorite}>
        <Icon name="heart" />
        <Text>Add to Favorites</Text>
      </Button>
    );
  }
}

export default FavoriteButton;
