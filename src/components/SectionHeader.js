/**
 * @format
 * @flow
 */
import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import { metrics, colors } from "../utils/themes";
import { H3, H2 } from "native-base";
import Colors from "../helpers/Colors";

class SectionHeader extends PureComponent {
  render() {
    const { title, right } = this.props;
    return (
      <View style={styles.container}>
        <H2 style={{fontWeight: 'bold', color: Colors.grey}}>{title}</H2>
        <View style={styles.divider} />
        {right}
      </View>
    );
  }
}

export default SectionHeader;

const styles = StyleSheet.create({
  container: {
    marginTop: metrics.padding,
    marginBottom: metrics.lessPadding,
    flexDirection: "row",
    alignItems: "center"
  },
  divider: {
    flex: 1,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: colors.divider,
    marginHorizontal: metrics.lessPadding
  }
});
