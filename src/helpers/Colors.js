import { Platform } from "react-native";

export default {
  blue: "#364E9A",
  black: "#000000",
  white: "#ffffff",
  grey: "#636e72",
  lightBlue: "#2DAAD8",
  yellow: "#F49F40",
  green: "#57BB94",
  lightOpacity: "rgba(255,255,255,0.8)",
  darkOpacity: "rgba(0, 0, 0, 0.1)",
  buttonBlue: Platform.OS === "ios" ? "#007aff" : "#3F51B5"
};
