import requestHandler, { instance } from "./controller";

const AudiobookService = {
  fetchAudiobooks: () => {
    return requestHandler("/customer/books", "get")
      .then(res => {
        return res;
      })
      .catch(e => {
        console.log(e);
      });
  },
  removeAudiobooks: () => {}
};

export default AudiobookService;
