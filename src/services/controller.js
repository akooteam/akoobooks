import axios from "axios";

// documentation http://api.akoobooks.net/api/v1/qwerty12345
const baseUrl = "https://api.akoobooks.net/api/v1";

export const instance = axios.create({
  baseURL: baseUrl
});

const controller = (url, method, params = null) => {
  switch (method) {
    case "get":
      return instance
        .get(url)
        .then(res => {
          return res.data;
        })
        .catch(e => {
          console.log("Get Error ", e);
        });

    case "post":
      return instance
        .post(url, params)
        .then(res => {
          return res.data;
        })
        .catch(e => {
          console.log("Post Error ", e);
        });

    case "patch":
      return instance
        .patch(url, params)
        .then(res => {
          return res.data;
        })
        .catch(e => {
          console.log("Patch Error ", e);
        });

    case "delete":
      return instance
        .delete(url)
        .then(res => {
          return res.data;
        })
        .catch(e => {
          console.log("Delete Error ", e);
        });

    default:
      return null;
  }
};

export default controller;
