import requestHandler, { instance } from "./controller";

const BookService = {
  newReleases: () => {
    return requestHandler("/books/new-release", "get").then(res => {
      return res;
    });
  },
  getBooks: id => {
    return requestHandler(`/genres/${id}/books`, "get").then(res => {
      return res;
    });
  },
  submitReview: payload => {
    return requestHandler(`/customer/books/${payload.id}/rate`, "post", {
      rating: payload.rating,
      review: payload.review
    }).then(res => {
      return res;
    });
  },
  getSingleBook: id => {
    return requestHandler(`/books/${id}/details`, "get").then(res => {
      return res;
    });
  },
  search: params => {
    return requestHandler(`/search?search_text=${params}`, "get").then(res => {
      return res;
    });
  }
};

export default BookService;
