import requestHandler, { instance } from "./controller";

const FavoriteService = {
  fetchFavorites: () => {
    return requestHandler("/customer/books/favourites", "get")
      .then(res => {
        return res;
      })
      .catch(e => {
        console.log(e);
      });
  },
  addFavorites: book_id => {
    return requestHandler(`/customer/books/${book_id}/favourites`, "post", {})
      .then(res => {
        return res;
      })
      .catch(e => {
        console.log(e);
      });
  },
  removeFavorites: book_id => {
    return requestHandler(`/customer/favorites/${book_id}`, "delete")
      .then(res => {
        return res;
      })
      .catch(e => {
        console.log(e);
      });
  }
};

export default FavoriteService;
