import requestHandler, { instance } from "./controller";

const UserService = {
  register: params => {
    return requestHandler("/customer/register", "post", params).then(res => {
      return res;
    });
  },
  updateProfile: params => {
    return requestHandler("/customer/update", "post", params).then(res => {
      if (res) {
        return res;
      }
      return false;
    });
  },
  setToken: token => {
    instance.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  },
  logout: () => {
    return requestHandler("/customer/logout", "post").then(res => {
      return res;
    });
  },
  customer: () => {
    return requestHandler("/customer", "get").then(res => {
      return res;
    });
  },
  push_notification: params => {
    return requestHandler("/customer/push-notification", "post", params).then(
      res => {
        return res;
      }
    );
  },
  login: params => {
    return requestHandler("/customer/login", "post", params).then(res => {
      return res;
    });
  },
  forgotPassword: params => {
    return requestHandler("/customer/reset", "post", params).then(res => {
      return res;
    });
  }
};

export default UserService;
