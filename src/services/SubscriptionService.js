import requestHandler, { instance } from "./controller";

const SubscriptionService = {
  getBooks: () => {
    return requestHandler("/customer/books/subscriptions", "get").then(res => {
      return res;
    });
  },
  addBook: bookId => {
    return requestHandler(
      `/customer/books/${bookId}/subscriptions`,
      "post"
    ).then(res => {
      return res;
    });
  },
  removeBook: bookId => {
    return requestHandler(`/customer/subscriptions/${bookId}`, "delete").then(
      res => {
        return res;
      }
    );
  },
  getPackages: () => {
    return requestHandler("/packages", "get").then(res => {
      return res;
    });
  }
};

export default SubscriptionService;
