import requestHandler, { instance } from "./controller";

const LibraryService = {
  fetchLibraries: () => {
    return requestHandler("/customer/books", "get").then(res => {
      return res;
    });
  }
};

export default LibraryService;
