import React, { Component } from "react";
import {
  createBottomTabNavigator,
  createStackNavigator,
  BottomTabBar,
  createSwitchNavigator
} from "react-navigation";
import { View, Text } from "react-native";
import { Icon } from "native-base";
import Colors from "../helpers/Colors";
import Discover from "../screens/Discover";
import Library from "../screens/Library";
import Search from "../screens/Search";
import Account from "../screens/Account";
import Favorites from "../screens/Favorites";
import Login from "../screens/Account/Login";
import Register from "../screens/Account/Register";
import PasswordReset from "../screens/Account/PasswordReset";
import MiniPlayer from "../components/MiniPlayer";
import Loading from "../components/Loading";
import Profile from "../screens/Account/Profile";
import BookDetails from "../screens/BookDetails";
import Reviews from "../screens/Reviews";
import { RootStore } from "../store";
// import { inject, observer } from "mobx-react/native";

const AuthStack = createStackNavigator(
  {
    Account: Account,
    Login: Login,
    Register: Register,
    PasswordReset: PasswordReset
  },
  {
    headerMode: "none",
    initialRouteName: "Account"
  }
);

const AccountStack = createSwitchNavigator({
  Loading: Loading,
  Auth: AuthStack,
  Profile: Profile
});

const TabBarComponent = props => {
  return (
    <View>
      <MiniPlayer {...props} />
      <BottomTabBar {...props} />
    </View>
  );
};

const DiscoverNav = createStackNavigator(
  {
    Discover: Discover,
    BookDetails: BookDetails,
    Reviews: Reviews
  },
  {
    headerMode: "none"
  }
);

const FavoritesNav = createStackNavigator(
  {
    Favorites: Favorites,
    BookDetails: BookDetails,
    Reviews: Reviews
  },
  {
    headerMode: "none"
  }
);

const LibraryNav = createStackNavigator(
  {
    Library: Library,
    BookDetails: BookDetails,
    Reviews: Reviews
  },
  {
    headerMode: "none"
  }
);

const SearchNav = createStackNavigator(
  {
    Search: Search,
    BookDetails: BookDetails,
    Reviews: Reviews
  },
  {
    headerMode: "none"
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Discover: DiscoverNav,
    Favorites: FavoritesNav,
    Library: LibraryNav,
    Search: SearchNav,
    Account: AccountStack
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Discover") {
          iconName = "folder-open";
        } else if (routeName === "Favorites") {
          iconName = `heart`;
        } else if (routeName === "Library") {
          iconName = `book-open`;
        } else if (routeName === "Search") {
          iconName = `search`;
        } else {
          iconName = "user";
        }

        // You can return any component that you like here!
        return (
          <Icon
            name={iconName}
            type="FontAwesome5"
            style={{
              color: tintColor
            }}
          />
        );
      }
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: Colors.lightBlue
    },
    tabBarComponent: props => (
      <TabBarComponent {...props} style={{ borderTopColor: "#605F60" }} />
    )
  }
);

export default TabNavigator;
