import React from "react";
import { createAppContainer, createStackNavigator } from "react-navigation";
import { Root } from "native-base";
import TabNavigator from "./TabNavigation";
import BookDetails from "../screens/BookDetails";
import Reviews from "../screens/Reviews";
import Player from "../screens/Player/Player";
import Playlist from "../screens/Playlist";
import ActiveBookDetails from "../screens/ActiveBookDetails";
import Settings from "../screens/Account/Settings";
import Memberships from "../screens/Account/Memberships";
import WebView from "../screens/WebView";

const RootStack = createStackNavigator(
  {
    Home: TabNavigator
    // BookDetails: BookDetails,
    // Reviews: Reviews
  },
  {
    headerMode: "none"
  }
);

const AppStack = createStackNavigator(
  {
    Main: RootStack,
    Settings: Settings,
    Memberships: Memberships,
    Player: Player,
    Playlist: Playlist,
    ActiveBookDetails: ActiveBookDetails,
    WebView: WebView
  },
  {
    headerMode: "none",
    mode: "modal",
    initialRouteName: "Main"
  }
);

const RootNavigator = createAppContainer(AppStack);

export default () => (
  <Root>
    <RootNavigator />
  </Root>
);
