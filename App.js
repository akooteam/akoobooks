/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { AsyncStorage, View, Alert } from "react-native";
import RootNavigation from "./src/routes";
import firebase from "react-native-firebase";
import TrackPlayer from "react-native-track-player";
import { applySnapshot } from "mobx-state-tree";
import { Provider } from "mobx-react/native";
import { RootStore } from "./src/store";
import UserService from "./src/services/UserService";

const appStatePersistenceKey = "appStatePersistenceKey";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };
    this.store = RootStore.create({});
  }

  async componentWillMount() {
    this._loadPersistedState();
  }

  componentDidMount() {
    this.checkPermission();
    this.createNotificationListeners();

    this._onTrackChanged = TrackPlayer.addEventListener(
      "playback-track-changed",
      async data => {
        if (data.nextTrack) {
          const track = await TrackPlayer.getTrack(data.nextTrack);
          this.store.playerStore.setTrack(track);
        }
      }
    );

    this._onStateChanged = TrackPlayer.addEventListener(
      "playback-state",
      async data => {
        if (data.state == TrackPlayer.STATE_PAUSED) {
          this.store.playerStore.setDuration(await TrackPlayer.getPosition());
        }

        this.store.playerStore.setPlayback(data.state);
      }
    );

    this._onPlaybackError = TrackPlayer.addEventListener(
      "playback-error",
      data => {
        Alert.alert("Error", "Unable to play audio at this time");
      }
    );

    this._onEndQueue = TrackPlayer.addEventListener(
      "playback-queue-ended",
      async data => {
        const { track, showPlayer, clearTrack } = this.store.playerStore;

        try {
          if (!track.purchased) {
            TrackPlayer.reset();
            showPlayer(false);
            clearTrack();
          }
        } catch (_) {}
      }
    );
  }

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const { title, body } = notification;
        this.showAlert(title, body);
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body } = notificationOpen.notification;
        this.showAlert(title, body);
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      // console.log(JSON.stringify(message));
    });
  }

  showAlert(title, body) {
    Alert.alert(title, body);
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();

    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem("fcmToken");

    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem("fcmToken", fcmToken);

        await UserService.push_notification({
          push_notification_id: fcmToken
        });
      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      Alert.alert("Notice", "Enable push notification to receive alerts.");
    }
  }

  componentWillUnmount() {
    this._onTrackChanged.remove();
    this._onStateChanged.remove();
    this._onEndQueue.remove();
    this._onPlaybackError.remove();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  _loadPersistedState = async () => {
    const retrievedState = await AsyncStorage.getItem(appStatePersistenceKey);

    if (retrievedState) {
      const rootStoreJson = JSON.parse(retrievedState);
      if (RootStore.is(rootStoreJson)) {
        applySnapshot(this.store, rootStoreJson);
      }
    }

    const { user, updateUser, createUser, logout } = this.store.userStore;

    if (user) {
      UserService.setToken(user.token);

      await UserService.customer()
        .then(res => {
          if (!res) return logout();
          createUser(res);
        })
        .catch(e => {
          logout();
        });
    }

    this.setState({ loading: false });
  };

  render() {
    if (this.state.loading) {
      return <View />;
    }

    return (
      <Provider rootStore={this.store}>
        <RootNavigation />
      </Provider>
    );
  }
}
